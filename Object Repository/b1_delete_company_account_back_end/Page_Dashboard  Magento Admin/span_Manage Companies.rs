<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Manage Companies</name>
   <tag></tag>
   <elementGuidId>a6354ae1-86ab-4851-86d0-11f7e72e40f3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='menu-magento-customer-customer']/div/ul/li[5]/div/ul/li/a/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.item-company-management.level-2 > a > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>770150bb-3686-4f0a-9380-c5c630f8eeca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Manage Companies</value>
      <webElementGuid>63649663-bb01-47e5-9387-c9845e4ead0b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;menu-magento-customer-customer&quot;)/div[@class=&quot;submenu&quot;]/ul[1]/li[@class=&quot;item-company-accounts  parent  level-1&quot;]/div[@class=&quot;submenu&quot;]/ul[1]/li[@class=&quot;item-company-management    level-2&quot;]/a[1]/span[1]</value>
      <webElementGuid>3bb2e9ce-8223-45a3-95d3-11534a7e1856</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='menu-magento-customer-customer']/div/ul/li[5]/div/ul/li/a/span</value>
      <webElementGuid>02b5f5df-07eb-45a3-b13b-c80034469177</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Amasty Company Accounts'])[1]/following::span[1]</value>
      <webElementGuid>6bdebd2f-46ed-43c8-acd6-61f5afeb6bc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Customer Groups'])[1]/following::span[2]</value>
      <webElementGuid>08a46cf5-1350-4f56-a8f1-559089b2110a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Marketing'])[1]/preceding::span[1]</value>
      <webElementGuid>69108be4-3b06-4496-bbeb-54619cfb9816</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Marketing'])[2]/preceding::span[2]</value>
      <webElementGuid>68a7f62d-2a99-4fff-bc92-303af0166e9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Manage Companies']/parent::*</value>
      <webElementGuid>ee2b0583-5819-4c68-9e29-f576178fcb4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/div/ul/li/a/span</value>
      <webElementGuid>8fda0c7f-5b14-4563-99d7-6724eaebdec9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Manage Companies' or . = 'Manage Companies')]</value>
      <webElementGuid>637cbe4d-2cd3-4563-95df-e3685fb6cf6d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
