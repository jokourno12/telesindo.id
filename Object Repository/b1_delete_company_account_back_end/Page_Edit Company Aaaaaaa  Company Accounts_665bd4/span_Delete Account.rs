<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Delete Account</name>
   <tag></tag>
   <elementGuidId>2f8a0b9d-195f-4cf9-9905-780e8ddbdcf4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='delete']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#delete > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2e672b07-a679-400d-8e74-136b57dbe150</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Delete Account</value>
      <webElementGuid>aad28c84-b35e-47b9-84ed-c54e0b5a7211</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;delete&quot;)/span[1]</value>
      <webElementGuid>61eaa706-35a0-4440-ae2b-652d517ead6f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='delete']/span</value>
      <webElementGuid>789c6457-31f7-4aa9-b4a9-52fe9b6aaf8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save&amp;Close'])[1]/following::span[1]</value>
      <webElementGuid>5d6171df-841b-4b1c-a13f-2d5e89137389</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save'])[1]/following::span[2]</value>
      <webElementGuid>d21886ed-99af-4357-b580-32b02c5e78fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='General'])[1]/preceding::span[9]</value>
      <webElementGuid>8d8da7ad-42f2-4d06-bc1c-96947abed58e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Company Name'])[1]/preceding::span[10]</value>
      <webElementGuid>176c2946-4045-4c2e-b07f-a55542b86b66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Delete Account']/parent::*</value>
      <webElementGuid>f3b46e6e-30bd-40fd-9a21-6e674dde5883</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[5]/span</value>
      <webElementGuid>1428a81f-96c8-46c9-aa91-7215d1b72e05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Delete Account' or . = 'Delete Account')]</value>
      <webElementGuid>f2af9d3c-1222-460f-9af0-1b535f390910</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
