<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Sign in</name>
   <tag></tag>
   <elementGuidId>e4aed9b0-c2d1-4717-84be-88396d63ea57</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='login-form']/fieldset/div[3]/div/button/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.action-login.action-primary > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>9fef346c-deeb-4c1c-acfd-e8fddbf707dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
      <webElementGuid>2eef0f27-6faf-4c5f-9319-5234cffc8fe8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login-form&quot;)/fieldset[@class=&quot;admin__fieldset&quot;]/div[@class=&quot;form-actions&quot;]/div[@class=&quot;actions&quot;]/button[@class=&quot;action-login action-primary&quot;]/span[1]</value>
      <webElementGuid>729328de-a31b-405f-9556-4f8e940ff625</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='login-form']/fieldset/div[3]/div/button/span</value>
      <webElementGuid>403ea7e4-a61b-47ad-a40e-09791d2f736f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::span[1]</value>
      <webElementGuid>6f21786c-5305-4a8d-bd6e-6305b8c94cf6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username'])[1]/following::span[2]</value>
      <webElementGuid>416e76a7-5819-4bc3-b520-8f354bd504c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot your password?'])[1]/preceding::span[1]</value>
      <webElementGuid>01afe9e4-ba24-4ade-bf84-c1639b060ef9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign in']/parent::*</value>
      <webElementGuid>b70bde51-cc1b-4964-9668-149640f030a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button/span</value>
      <webElementGuid>596498b9-2d6c-4225-b062-ca5f00c5f46f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Sign in' or . = 'Sign in')]</value>
      <webElementGuid>1eb638b3-adc6-4fd8-b2ee-3bb5175a792d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
