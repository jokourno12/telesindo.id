<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Company Name</name>
   <tag></tag>
   <elementGuidId>55bd1543-0cbf-4d48-bbd7-5cc89eb11923</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div[3]/table/thead/tr/th[3]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>th.data-grid-th._sortable._draggable._ascend > span.data-grid-cell-content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>1eb19785-49cb-4121-ab76-eeb441473f20</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>data-grid-cell-content</value>
      <webElementGuid>e829d3ac-b685-45f6-b584-71facf5a258c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>i18n: label</value>
      <webElementGuid>a319a7e7-c78e-4646-88df-2d680ca5a757</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Company Name</value>
      <webElementGuid>de436d99-93cb-4c41-886d-5662e6632c84</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[@class=&quot;admin__data-grid-outer-wrap&quot;]/div[@class=&quot;admin__data-grid-wrap&quot;]/table[@class=&quot;data-grid data-grid-draggable&quot;]/thead[1]/tr[1]/th[@class=&quot;data-grid-th _sortable _draggable _ascend&quot;]/span[@class=&quot;data-grid-cell-content&quot;]</value>
      <webElementGuid>72d0f342-4c16-4f53-9d72-e704ae2fba82</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div[3]/table/thead/tr/th[3]/span</value>
      <webElementGuid>745d65fb-c746-4a9e-b555-4a1918d5bfa8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ID'])[3]/following::span[1]</value>
      <webElementGuid>1c347f6e-1f5f-48c0-b52c-8ae792110638</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deselect All on This Page'])[1]/following::span[2]</value>
      <webElementGuid>d59a3d32-1dd2-45a7-a26f-c395db7cf0b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[3]/preceding::span[1]</value>
      <webElementGuid>af999184-6e95-487f-8296-52c754868d29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone Number'])[3]/preceding::span[2]</value>
      <webElementGuid>8a38739d-96fc-4b15-b37a-24fb1fbdfc6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//th[3]/span</value>
      <webElementGuid>bf0dab0a-0557-479f-9021-2067f59437f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Company Name' or . = 'Company Name')]</value>
      <webElementGuid>6cd784a7-41e5-4e7e-9859-4655ca3432a1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
