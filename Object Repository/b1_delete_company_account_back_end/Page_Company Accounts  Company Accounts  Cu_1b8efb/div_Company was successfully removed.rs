<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Company was successfully removed</name>
   <tag></tag>
   <elementGuidId>c69ad03e-8366-4872-8ff4-4f7c1dd8b408</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='messages']/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.message.message-success.success > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c0766655-afc0-4877-aaa7-0bbcf84f8057</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ui-id</name>
      <type>Main</type>
      <value>messages-message-success</value>
      <webElementGuid>0b138040-c991-48d6-bc29-1c4d2855b2b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Company was successfully removed</value>
      <webElementGuid>c1cc3a1c-076d-4916-9c03-38958bdb7208</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;messages&quot;)/div[@class=&quot;messages&quot;]/div[@class=&quot;message message-success success&quot;]/div[1]</value>
      <webElementGuid>7207374c-ac20-4e7e-a844-639f19b9430c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='messages']/div/div/div</value>
      <webElementGuid>fae68e1e-7a0c-4caa-8a61-21f3355ad61d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add New Company'])[1]/following::div[4]</value>
      <webElementGuid>b9ff4c40-3683-485d-8cf7-5742ec553fef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Default View'])[1]/preceding::div[3]</value>
      <webElementGuid>5818f7f0-7484-4744-b57a-b6aa09a0932c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Default View'])[2]/preceding::div[3]</value>
      <webElementGuid>c4033208-525d-4e9f-93b7-4f52ad081430</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Company was successfully removed']/parent::*</value>
      <webElementGuid>51226dd6-9cd4-41c8-a627-50e140f2a422</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//main/div[2]/div/div/div</value>
      <webElementGuid>943a99e5-390e-42cc-9707-ccabc8b6acd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Company was successfully removed' or . = 'Company was successfully removed')]</value>
      <webElementGuid>d6df0b4a-8830-4eef-9b34-ee01621fce38</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
