<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Aaaaaaa</name>
   <tag></tag>
   <elementGuidId>000e9436-d9d8-480a-8e5c-fce6d0f558e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div[3]/table/tbody/tr[2]/td[3]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td:nth-of-type(3) > div.data-grid-cell-content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f0aa3d8f-502d-412c-8f5b-c918d6611c8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>data-grid-cell-content</value>
      <webElementGuid>1df5443a-5735-4246-a40e-f7c346d2edcb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>text: $col.getLabel($row())</value>
      <webElementGuid>7fcfa55d-8205-4719-8f56-74dfc741a573</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Aaaaaaa</value>
      <webElementGuid>c2b686f0-43e1-4975-839d-6b6bef8f009d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[@class=&quot;admin__data-grid-outer-wrap&quot;]/div[@class=&quot;admin__data-grid-wrap&quot;]/table[@class=&quot;data-grid data-grid-draggable&quot;]/tbody[1]/tr[@class=&quot;data-row&quot;]/td[3]/div[@class=&quot;data-grid-cell-content&quot;]</value>
      <webElementGuid>ce7572db-7562-4080-8a5e-6d75986cf296</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div[3]/table/tbody/tr[2]/td[3]/div</value>
      <webElementGuid>80fd2538-a883-4cc1-882d-bbc17c7eac3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[1]/following::div[2]</value>
      <webElementGuid>a70da86d-bc32-420e-bdf8-ef130782a5a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[2]/following::div[2]</value>
      <webElementGuid>eb54ed14-5679-4517-8bbd-81488ced1c70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Active'])[2]/preceding::div[1]</value>
      <webElementGuid>04353c89-70a2-4832-b945-8522c85422be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indonesia'])[2]/preceding::div[3]</value>
      <webElementGuid>f350ca2e-d252-4d20-9a47-a4878ce96160</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Aaaaaaa']/parent::*</value>
      <webElementGuid>c4edf671-99d4-41ab-97ca-3cce9ad141c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]/div</value>
      <webElementGuid>94c55bd1-6219-462b-9c71-4b1847bcb9f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Aaaaaaa' or . = 'Aaaaaaa')]</value>
      <webElementGuid>63b3980e-43dc-4dbe-afb2-755bf0ccd760</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
