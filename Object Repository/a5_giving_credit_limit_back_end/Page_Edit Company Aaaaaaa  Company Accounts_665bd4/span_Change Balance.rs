<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Change Balance</name>
   <tag></tag>
   <elementGuidId>100ffd3b-f353-4300-938d-6f956aa18d6f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.amcompany-button.-clear > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div[2]/div[6]/div[2]/fieldset/div[2]/div[3]/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>02b2e3ff-ff1b-4db7-8bdb-a204b756c32c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>i18n: 'Change Balance'</value>
      <webElementGuid>8dd70677-5ae2-4b4e-8426-23d33813d134</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Change Balance</value>
      <webElementGuid>02617e9a-d313-454d-b99d-e68875fd0ce1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[1]/div[@class=&quot;entry-edit form-inline&quot;]/div[@class=&quot;fieldset-wrapper admin__collapsible-block-wrapper _show&quot;]/div[@class=&quot;admin__fieldset-wrapper-content admin__collapsible-content _show&quot;]/fieldset[@class=&quot;admin__fieldset&quot;]/div[@class=&quot;amcompany-paycard-block -balance-card&quot;]/div[@class=&quot;amcompany-actions&quot;]/button[@class=&quot;amcompany-button -clear&quot;]/span[1]</value>
      <webElementGuid>969f8474-643f-40b3-9815-e74d90803275</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div[2]/div[6]/div[2]/fieldset/div[2]/div[3]/button/span</value>
      <webElementGuid>aae27188-0376-4dbe-a115-733d8b07a997</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[1]/following::span[1]</value>
      <webElementGuid>a905b735-48dc-42bb-a8a8-465658bf45a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 0'])[2]/following::span[1]</value>
      <webElementGuid>c3cac5ee-d784-445e-96d1-36d33b2d0ae4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='To Be Paid'])[1]/preceding::span[1]</value>
      <webElementGuid>ba3170f2-8dd7-4464-ad2f-f3464b5216d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 0'])[3]/preceding::span[2]</value>
      <webElementGuid>c170ca52-dc5c-4298-9d5b-c0e5cff6be2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Change Balance']/parent::*</value>
      <webElementGuid>9d79bad0-3d4d-4fe4-b142-cd85c6303502</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button/span</value>
      <webElementGuid>19e35ce7-098b-437f-9c8b-ed6dc15c18ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Change Balance' or . = 'Change Balance')]</value>
      <webElementGuid>f12551d9-3347-404b-9266-697f2953c818</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
