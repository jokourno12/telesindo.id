<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Amount_store_creditchange_balanceamount</name>
   <tag></tag>
   <elementGuidId>a9ec3215-0728-451c-96a4-7dadd5fa4681</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#LQ6C11M</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='store_credit[change_balance][amount]']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>a5c8644f-e50d-4b49-959c-995a9a38c25d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>admin__control-text</value>
      <webElementGuid>4e9d55f4-596e-44bd-84fa-2e75b36dc3df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>1e0bac98-4095-4e87-8046-0659df353d1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>
        event: {change: userChanges},
        value: value,
        hasFocus: focused,
        valueUpdate: valueUpdate,
        attr: {
            name: inputName,
            placeholder: placeholder,
            'aria-describedby': noticeId,
            id: uid,
            disabled: disabled,
            maxlength: 255
    }</value>
      <webElementGuid>b59bc8b5-c4ab-489c-9376-dd3e597d7f39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>store_credit[change_balance][amount]</value>
      <webElementGuid>e0ca0000-f9ad-49b1-810b-4fd165a6f19f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>notice-LQ6C11M</value>
      <webElementGuid>6614dba3-fa07-44d2-9a3a-40645eee4177</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>LQ6C11M</value>
      <webElementGuid>1dd3bd5e-3e91-4c19-8943-feabccb545e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>255</value>
      <webElementGuid>26988fea-930e-471a-a401-71ba29c6635c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;LQ6C11M&quot;)</value>
      <webElementGuid>acc25859-8e33-4dae-b875-885b7e6ead4f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='LQ6C11M']</value>
      <webElementGuid>36b4471d-8821-46bc-90fb-7875600e5f86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modal-content-22']/div/div/div/fieldset/div/div[2]/div/input</value>
      <webElementGuid>8293da1d-9354-4fbf-8911-6a3d9d92addf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/fieldset/div/div[2]/div/input</value>
      <webElementGuid>660c9417-0576-4db3-8979-abde3eaeb6af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'store_credit[change_balance][amount]' and @id = 'LQ6C11M']</value>
      <webElementGuid>3df0acb9-6ef1-4400-9498-05e9b19da8d3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
