<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Store Credit</name>
   <tag></tag>
   <elementGuidId>eceee979-0cfa-4835-9279-ccc7bf6bedb4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div[2]/div[6]/div/strong/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>968184f2-1df3-49c8-be39-9a390836f7fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>i18n: label</value>
      <webElementGuid>4c79306e-88e2-4190-9cf8-55edef9a40aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Store Credit</value>
      <webElementGuid>cd56d9dc-d298-4df8-bf0e-293176260a47</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[1]/div[@class=&quot;entry-edit form-inline&quot;]/div[@class=&quot;fieldset-wrapper admin__collapsible-block-wrapper&quot;]/div[@class=&quot;fieldset-wrapper-title&quot;]/strong[@class=&quot;admin__collapsible-title&quot;]/span[1]</value>
      <webElementGuid>bd728ddb-497b-4dae-a4f9-cf8aebef186c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div[2]/div[6]/div/strong/span</value>
      <webElementGuid>1686e8de-c1e0-47b6-b180-724ca0581f97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='This tab contains invalid data. Please resolve this before saving.'])[4]/following::span[11]</value>
      <webElementGuid>507505a8-1cb4-4ecc-9e1e-020530d01d03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Changes have been made to this section that have not been saved.'])[4]/following::span[14]</value>
      <webElementGuid>d640dc09-c1a9-4893-a819-9ce488decaa8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Changes have been made to this section that have not been saved.'])[5]/preceding::span[2]</value>
      <webElementGuid>e8cca332-c530-4406-9d51-00bf6c1bbd6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='This tab contains invalid data. Please resolve this before saving.'])[5]/preceding::span[5]</value>
      <webElementGuid>7cd02ccf-cf67-4da5-99e5-32e69c873aaa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Store Credit']/parent::*</value>
      <webElementGuid>c25e36a4-00d1-4595-a03f-93f6c2322b28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/strong/span</value>
      <webElementGuid>3b1a7837-1ef1-4516-9ca2-e80487ba3eb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Store Credit' or . = 'Store Credit')]</value>
      <webElementGuid>ef0ac8e1-f619-4032-8485-0d974194cb21</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
