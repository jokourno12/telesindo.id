<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Rp 12,000,000</name>
   <tag></tag>
   <elementGuidId>1f1c73e4-9072-4e1f-9e05-7c361ec5c310</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.amcompany-item > span.amcompany-value</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div[2]/div[6]/div[2]/fieldset/div[2]/div[2]/ul/li/span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>576e60b7-1a0a-4521-9dfb-5a5a523022ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>amcompany-value</value>
      <webElementGuid>5593c920-9545-4474-9e5f-6dc494b8bd00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>text: credit</value>
      <webElementGuid>98ed53d1-a7b4-4c22-8026-3fdc741d46e1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Rp 12,000,000</value>
      <webElementGuid>b4781a19-23f2-4d1c-8063-b24b4a2d2d9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[1]/div[@class=&quot;entry-edit form-inline&quot;]/div[@class=&quot;fieldset-wrapper admin__collapsible-block-wrapper _show&quot;]/div[@class=&quot;admin__fieldset-wrapper-content admin__collapsible-content _show&quot;]/fieldset[@class=&quot;admin__fieldset&quot;]/div[@class=&quot;amcompany-paycard-block -balance-card&quot;]/div[@class=&quot;amcompany-content&quot;]/ul[@class=&quot;amcompany-list-block&quot;]/li[@class=&quot;amcompany-item&quot;]/span[@class=&quot;amcompany-value&quot;]</value>
      <webElementGuid>e2fb2688-b4af-4211-a901-c860c24b5dcd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div[2]/div[6]/div[2]/fieldset/div[2]/div[2]/ul/li/span[2]</value>
      <webElementGuid>9d27339b-7331-4705-a9e3-59413887c44c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Issued Credit:'])[1]/following::span[1]</value>
      <webElementGuid>2f1f872d-ca2c-44bc-a25c-4e35f263606a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 12,000,000'])[1]/following::span[3]</value>
      <webElementGuid>64927534-eb33-485f-8b23-7b34a062e029</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+'])[1]/preceding::span[1]</value>
      <webElementGuid>8d677793-a802-4684-9caf-f0570042ede4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Balance'])[1]/preceding::span[1]</value>
      <webElementGuid>c0571abb-52bd-4303-96c1-ecd217e24a2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/span[2]</value>
      <webElementGuid>c8747efb-e745-463c-a8e8-34918fa44ff2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Rp 12,000,000' or . = 'Rp 12,000,000')]</value>
      <webElementGuid>f1300504-6124-4daa-a6ae-05cdb458dae3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
