<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Sign in</name>
   <tag></tag>
   <elementGuidId>9cfd360f-150b-41d4-a358-f6baff736c74</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.action-login.action-primary > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='login-form']/fieldset/div[3]/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>4e9f6c32-ce9c-4fcd-92d0-b69cf00740fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
      <webElementGuid>fe7fe7cf-e160-4e75-9772-0571d76ec5cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login-form&quot;)/fieldset[@class=&quot;admin__fieldset&quot;]/div[@class=&quot;form-actions&quot;]/div[@class=&quot;actions&quot;]/button[@class=&quot;action-login action-primary&quot;]/span[1]</value>
      <webElementGuid>d0ae7d2e-7f0d-441a-8a84-590bb3879c33</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='login-form']/fieldset/div[3]/div/button/span</value>
      <webElementGuid>ccf3f4f3-629d-48c5-894e-4aceb10f769f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::span[1]</value>
      <webElementGuid>7993712a-a4f2-410c-83da-b59a493a2c56</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username'])[1]/following::span[2]</value>
      <webElementGuid>89af1db4-a394-4e1e-beda-9ce060569ff7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot your password?'])[1]/preceding::span[1]</value>
      <webElementGuid>399ec49c-44dc-41ab-b109-94c468992e7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign in']/parent::*</value>
      <webElementGuid>1d1475ba-dba0-41cf-9c36-fc4230dc1a5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button/span</value>
      <webElementGuid>89af961e-5413-4f86-a3ef-8132a9bbfd57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Sign in' or . = 'Sign in')]</value>
      <webElementGuid>c6dea653-6d3e-4f2c-89a3-6cd815ae55da</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
