<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Manage Companies</name>
   <tag></tag>
   <elementGuidId>cca4a7f2-8bf1-4f82-aa43-902d262b28a0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.item-company-management.level-2 > a > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='menu-magento-customer-customer']/div/ul/li[5]/div/ul/li/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>d97a6431-edd6-46c4-a452-a97c949f0547</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Manage Companies</value>
      <webElementGuid>eabaa2cb-5423-4776-8786-9fd027897313</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;menu-magento-customer-customer&quot;)/div[@class=&quot;submenu&quot;]/ul[1]/li[@class=&quot;item-company-accounts  parent  level-1&quot;]/div[@class=&quot;submenu&quot;]/ul[1]/li[@class=&quot;item-company-management    level-2&quot;]/a[1]/span[1]</value>
      <webElementGuid>7f28afe0-38fe-4d57-af45-fd7a3cbae373</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='menu-magento-customer-customer']/div/ul/li[5]/div/ul/li/a/span</value>
      <webElementGuid>48b968b8-fdc3-43ea-9021-7b188507ed4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Amasty Company Accounts'])[1]/following::span[1]</value>
      <webElementGuid>1b625962-1b4f-4b23-8d37-bd512e2857eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Customer Groups'])[1]/following::span[2]</value>
      <webElementGuid>6deee613-22b8-4499-bb6a-6e7d8cbad921</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Marketing'])[1]/preceding::span[1]</value>
      <webElementGuid>38cd4523-fa6d-4e28-85fc-cf55a6641736</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Marketing'])[2]/preceding::span[2]</value>
      <webElementGuid>ebf811df-3160-4232-8aa7-a2350563e931</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Manage Companies']/parent::*</value>
      <webElementGuid>534a2557-b3dc-4a11-878b-8d3ea3e7f53d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/div/ul/li/a/span</value>
      <webElementGuid>ce5f89d2-f336-45b5-92b0-b2870df0411a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Manage Companies' or . = 'Manage Companies')]</value>
      <webElementGuid>fe3d6cc6-bca8-435d-ae3f-6bc8b21a8233</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
