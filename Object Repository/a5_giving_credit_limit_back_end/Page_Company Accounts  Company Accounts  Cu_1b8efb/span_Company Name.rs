<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Company Name</name>
   <tag></tag>
   <elementGuidId>2d1db79f-3078-41fb-9e30-749d5a9dbee9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>th.data-grid-th._sortable._draggable._ascend > span.data-grid-cell-content</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div[3]/table/thead/tr/th[3]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>c0df2fe8-f95f-4d7d-b035-53142230ad7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>data-grid-cell-content</value>
      <webElementGuid>04e697a6-fc11-48e6-ad49-3a2c6bebb120</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>i18n: label</value>
      <webElementGuid>53482844-34f4-429c-acaf-01e13f3020da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Company Name</value>
      <webElementGuid>98becca9-267e-4c3b-ae50-fc9b9b71544f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[@class=&quot;admin__data-grid-outer-wrap&quot;]/div[@class=&quot;admin__data-grid-wrap&quot;]/table[@class=&quot;data-grid data-grid-draggable&quot;]/thead[1]/tr[1]/th[@class=&quot;data-grid-th _sortable _draggable _ascend&quot;]/span[@class=&quot;data-grid-cell-content&quot;]</value>
      <webElementGuid>cc4a5934-e017-430f-bb45-e3f176123adb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div[3]/table/thead/tr/th[3]/span</value>
      <webElementGuid>890d9830-5553-4d95-adf5-d1332b643376</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ID'])[3]/following::span[1]</value>
      <webElementGuid>2b78a27c-04a7-49ea-9d18-75f526c43a48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deselect All on This Page'])[1]/following::span[2]</value>
      <webElementGuid>f4c249e6-74e1-433e-8b30-59de694e81d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[3]/preceding::span[1]</value>
      <webElementGuid>793ff340-9ada-4529-b243-9183d276df11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone Number'])[3]/preceding::span[2]</value>
      <webElementGuid>88afa958-fb38-435e-953e-4905fb1819e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//th[3]/span</value>
      <webElementGuid>2ed2f6e9-23c7-4f8e-86b6-67dba4995c46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Company Name' or . = 'Company Name')]</value>
      <webElementGuid>53766dce-bfb2-4de6-8066-8efd3c6eb492</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
