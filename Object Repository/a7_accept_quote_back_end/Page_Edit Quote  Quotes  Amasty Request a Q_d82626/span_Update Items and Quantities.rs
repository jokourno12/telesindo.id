<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Update Items and Quantities</name>
   <tag></tag>
   <elementGuidId>dade0eba-a177-4662-8670-83e3a4ffc0fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#id_BqfgDmNOiAt4mL7kUetyI5ZOFDE1Axz3 > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(., 'Update Items and Quantities')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>6ab63347-f902-4772-9b5d-3cb87622e860</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Update Items and Quantities</value>
      <webElementGuid>2d97c3cd-e9c5-4e34-bba6-b5ddf4dfbe56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;id_BqfgDmNOiAt4mL7kUetyI5ZOFDE1Axz3&quot;)/span[1]</value>
      <webElementGuid>835b941f-46a6-40b3-b21a-f23155903446</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='id_BqfgDmNOiAt4mL7kUetyI5ZOFDE1Axz3']/span</value>
      <webElementGuid>dd413b6c-2cf1-4ef1-9ece-55c10d2181b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Additional Surcharge (%)'])[1]/following::span[1]</value>
      <webElementGuid>9f079c8e-5df5-4f66-b34e-0ac26b026df2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Additional Discount (%)'])[1]/following::span[1]</value>
      <webElementGuid>f6e1545a-9eb8-4137-b4ab-72827c082e3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Configure Shipping'])[1]/preceding::span[1]</value>
      <webElementGuid>0f8d50f2-c9b1-43fe-96a2-3768c9a5e94b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Address Information'])[1]/preceding::span[1]</value>
      <webElementGuid>25c0808e-09d3-4773-9fe0-db66d25ddb93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Update Items and Quantities']/parent::*</value>
      <webElementGuid>9143e676-0bea-4139-b8c6-64eab4e228fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button/span</value>
      <webElementGuid>85ff86d0-f910-45a7-aa94-2c4f5be62aa0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Update Items and Quantities' or . = 'Update Items and Quantities')]</value>
      <webElementGuid>17e841ec-a566-4bd1-84ff-ad1e4b9d5bb1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
