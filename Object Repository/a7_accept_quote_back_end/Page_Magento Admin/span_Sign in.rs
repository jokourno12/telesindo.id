<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Sign in</name>
   <tag></tag>
   <elementGuidId>95c98cd5-87b0-4e32-951c-18779e3e2311</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.action-login.action-primary > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='login-form']/fieldset/div[3]/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>99026158-a7d5-4e2d-8f1e-c2e7c1e44b2d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sign in</value>
      <webElementGuid>ea884281-42aa-4e79-a642-5f8e56947f15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login-form&quot;)/fieldset[@class=&quot;admin__fieldset&quot;]/div[@class=&quot;form-actions&quot;]/div[@class=&quot;actions&quot;]/button[@class=&quot;action-login action-primary&quot;]/span[1]</value>
      <webElementGuid>945118b5-ce45-4be6-999f-a2a453ae5ff3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='login-form']/fieldset/div[3]/div/button/span</value>
      <webElementGuid>c2b78833-b8a4-4703-85f3-1bac9e317cba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::span[1]</value>
      <webElementGuid>69c0c7d2-b04f-46c6-b51b-dfe530fd4e51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username'])[1]/following::span[2]</value>
      <webElementGuid>0dc90998-b3d9-4e54-bbe3-208823aa194c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot your password?'])[1]/preceding::span[1]</value>
      <webElementGuid>3787b21b-1465-450c-89d4-cfb3ce633276</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign in']/parent::*</value>
      <webElementGuid>b5ef16f1-8a23-4b4b-a25b-3ddc72a343e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button/span</value>
      <webElementGuid>1022cef3-0cdf-42a9-8a66-324b78ac6db3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Sign in' or . = 'Sign in')]</value>
      <webElementGuid>c8ee0917-d3cb-42ef-9c13-435f6a4aafd1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
