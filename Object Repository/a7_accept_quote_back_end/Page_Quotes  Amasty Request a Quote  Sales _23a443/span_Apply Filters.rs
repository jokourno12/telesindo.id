<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Apply Filters</name>
   <tag></tag>
   <elementGuidId>7d3174d7-add3-409b-b7cf-794db3dcd36a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.action-secondary > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div[2]/div/div[4]/div/div/button[2]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>731974b8-5e81-4cc9-b0a6-0e73a39ae123</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>i18n: 'Apply Filters'</value>
      <webElementGuid>8c77319e-5baa-4d54-a9eb-e8f5c48e0e4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Apply Filters</value>
      <webElementGuid>97cbf511-e602-453e-8cec-edad80f80b21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[@class=&quot;admin__data-grid-outer-wrap&quot;]/div[@class=&quot;admin__data-grid-header&quot;]/div[@class=&quot;admin__data-grid-header-row&quot;]/div[@class=&quot;admin__data-grid-filters-wrap _show&quot;]/div[@class=&quot;admin__data-grid-filters-footer&quot;]/div[@class=&quot;admin__footer-main-actions&quot;]/button[@class=&quot;action-secondary&quot;]/span[1]</value>
      <webElementGuid>c9ca418b-2aae-47c5-93d0-53198132866f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div[2]/div/div[4]/div/div/button[2]/span</value>
      <webElementGuid>ec3ccea6-e845-42f9-9b36-e2bdd7d7dd67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[3]/following::span[1]</value>
      <webElementGuid>95e8e10c-fa9d-454c-a463-295e42a696a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[2]/following::span[2]</value>
      <webElementGuid>fdf1e187-c725-4418-9b57-1daabaf4178b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[1]/preceding::span[1]</value>
      <webElementGuid>b2f88a10-8526-44eb-b0a7-216a66b152df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel / Close'])[1]/preceding::span[2]</value>
      <webElementGuid>e34dc46c-f751-4031-a4f1-af00ffc63411</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Apply Filters']/parent::*</value>
      <webElementGuid>1ed9917c-e079-4729-8a34-0f9a1c0355ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]/span</value>
      <webElementGuid>dd5f65d3-8b72-4947-a560-53639a0dcc51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Apply Filters' or . = 'Apply Filters')]</value>
      <webElementGuid>6bd41d0d-b3dd-4eb7-87e6-0084419ec970</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
