<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_You updated the quote</name>
   <tag></tag>
   <elementGuidId>37641b34-181b-46ad-9c4b-ca20fc7be6d8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.message.message-success.success</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='messages']/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3090371f-8511-4c93-a792-397d2790e9a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>message message-success success</value>
      <webElementGuid>b06a4f15-a4ff-4454-bd2b-15b6234377ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>You updated the quote.</value>
      <webElementGuid>ba303a7c-7d47-4cfe-b328-643788acd5ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;messages&quot;)/div[@class=&quot;messages&quot;]/div[@class=&quot;message message-success success&quot;]</value>
      <webElementGuid>1e912e4e-9a3b-4418-bdeb-232dd0e17bb1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='messages']/div/div</value>
      <webElementGuid>67a2ddca-d034-4c1d-99e1-21ba60dae905</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::div[3]</value>
      <webElementGuid>890ebe31-fa7f-434a-a766-251d892426da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download PDF'])[1]/following::div[3]</value>
      <webElementGuid>3dc1acc0-9803-4e4b-be3d-38d5a0d21de2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quote &amp; Account Information'])[1]/preceding::div[3]</value>
      <webElementGuid>055e6192-33f6-4624-9610-90c61a71eba0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//main/div[2]/div/div</value>
      <webElementGuid>0047949f-acff-4020-b6ad-fb54e1f0f34a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'You updated the quote.' or . = 'You updated the quote.')]</value>
      <webElementGuid>754a868b-9c33-45f4-aedd-01bf36f45b0d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
