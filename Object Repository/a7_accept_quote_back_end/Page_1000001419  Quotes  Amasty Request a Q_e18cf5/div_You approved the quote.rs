<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_You approved the quote</name>
   <tag></tag>
   <elementGuidId>c8f3a002-938f-4457-8e79-f402e31ce9ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.message.message-success.success > div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='messages']/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6833fb2a-d86f-4352-a6a4-974f60d8cf41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ui-id</name>
      <type>Main</type>
      <value>messages-message-success</value>
      <webElementGuid>759309e4-ebd9-45aa-9518-aed2916101b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>You approved the quote.</value>
      <webElementGuid>ef7c789d-0372-437c-9512-5c0d52302430</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;messages&quot;)/div[@class=&quot;messages&quot;]/div[@class=&quot;message message-success success&quot;]/div[1]</value>
      <webElementGuid>aeffd039-c00b-4376-84cd-34fb9a14c0f9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='messages']/div/div/div</value>
      <webElementGuid>0f8e1071-dc27-4f90-8bae-0b1ce79b9866</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::div[4]</value>
      <webElementGuid>6c8f9895-401e-41d6-ac03-549911b1bf18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Download PDF'])[1]/following::div[4]</value>
      <webElementGuid>cf80bfab-41d3-4c20-8e5c-9da02ea5671e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quote &amp; Account Information'])[1]/preceding::div[2]</value>
      <webElementGuid>d6d76ca2-7836-4bde-9c9d-8dcfad14da65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quote # 1000001419'])[1]/preceding::div[3]</value>
      <webElementGuid>75b70470-df96-4ba4-a0d2-40546440e261</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='You approved the quote.']/parent::*</value>
      <webElementGuid>e3fb9685-1702-4f2c-b13b-64909bca06a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//main/div[2]/div/div/div</value>
      <webElementGuid>5bf09047-4506-4cd1-819e-b5f50a839d33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'You approved the quote.' or . = 'You approved the quote.')]</value>
      <webElementGuid>31c400da-6154-4a10-8fe7-e32d2255b80d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
