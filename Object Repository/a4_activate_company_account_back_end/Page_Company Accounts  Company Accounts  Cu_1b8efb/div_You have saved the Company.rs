<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_You have saved the Company</name>
   <tag></tag>
   <elementGuidId>983d4c33-d5b9-473e-bfa6-c611a77c9c0a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.message.message-success.success > div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='messages']/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>14831d86-dea7-4c0b-8045-66a9aa9392ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ui-id</name>
      <type>Main</type>
      <value>messages-message-success</value>
      <webElementGuid>dd3aa212-a34f-47f5-a192-e1e388963a98</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>You have saved the Company.</value>
      <webElementGuid>5a1fe253-dc67-4d95-9862-a800997157b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;messages&quot;)/div[@class=&quot;messages&quot;]/div[@class=&quot;message message-success success&quot;]/div[1]</value>
      <webElementGuid>ace4bb5b-818b-4c17-a88c-41fdaed76ccc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='messages']/div/div/div</value>
      <webElementGuid>76f78780-0d19-43b1-b7f9-9e847be02a59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add New Company'])[1]/following::div[4]</value>
      <webElementGuid>1cdfcbb9-e71c-4e68-bf3d-009c67e35380</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Default View'])[1]/preceding::div[3]</value>
      <webElementGuid>278c1857-b044-48a2-b1bf-f8313c324253</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Default View'])[2]/preceding::div[3]</value>
      <webElementGuid>a2f43d9c-e486-4adf-b9b6-439f53df9bcf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='You have saved the Company.']/parent::*</value>
      <webElementGuid>d769860f-a6c2-4766-afd4-3ac04974869f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//main/div[2]/div/div/div</value>
      <webElementGuid>3644ee3a-163a-4721-be77-7ce5c9e24faf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'You have saved the Company.' or . = 'You have saved the Company.')]</value>
      <webElementGuid>4f89615b-c7b7-480b-8cc2-7a9599c7f22f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
