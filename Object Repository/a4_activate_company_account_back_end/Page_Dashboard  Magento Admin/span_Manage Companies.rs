<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Manage Companies</name>
   <tag></tag>
   <elementGuidId>b46a741a-cd0a-4050-b57d-11a2389d0dae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.item-company-management.level-2 > a > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='menu-magento-customer-customer']/div/ul/li[5]/div/ul/li/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2bd9e6bf-219a-405c-a174-64d8ee583aef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Manage Companies</value>
      <webElementGuid>a3fadf20-7d4e-4f55-8ff2-3e0c0b59cffb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;menu-magento-customer-customer&quot;)/div[@class=&quot;submenu&quot;]/ul[1]/li[@class=&quot;item-company-accounts  parent  level-1&quot;]/div[@class=&quot;submenu&quot;]/ul[1]/li[@class=&quot;item-company-management    level-2&quot;]/a[1]/span[1]</value>
      <webElementGuid>1a6e3ae7-61ff-46f2-9053-f34669d354ab</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='menu-magento-customer-customer']/div/ul/li[5]/div/ul/li/a/span</value>
      <webElementGuid>c46824da-7ca4-4c26-b8d1-30674239d8aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Amasty Company Accounts'])[1]/following::span[1]</value>
      <webElementGuid>7baa36ea-3823-4f49-bc43-78bb82a3d201</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Customer Groups'])[1]/following::span[2]</value>
      <webElementGuid>64d983fc-205c-4935-bab7-eb510247bff5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Marketing'])[1]/preceding::span[1]</value>
      <webElementGuid>1a027ade-b1ad-4e1d-9ac7-d979167ff55e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Marketing'])[2]/preceding::span[2]</value>
      <webElementGuid>06e68568-dfe8-46f9-a5cf-4a10429de858</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Manage Companies']/parent::*</value>
      <webElementGuid>752d447c-b887-4407-a126-7cabbf52a7d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/div/ul/li/a/span</value>
      <webElementGuid>6ba3a646-f04e-40dd-9f7a-3a664e33685c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Manage Companies' or . = 'Manage Companies')]</value>
      <webElementGuid>70b65230-042b-4071-a610-94db6364d0cb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
