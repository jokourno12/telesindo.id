<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_PendingInactiveActiveRejected</name>
   <tag></tag>
   <elementGuidId>a64c9224-ee54-45e5-8338-25e3176f0016</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#SBQMQAD</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='status']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>6c6519cd-b581-4fe2-a51f-5274c51beb18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>admin__control-select</value>
      <webElementGuid>9b7e3aea-cafd-499a-998b-32f22f9e7b2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>
    attr: {
        name: inputName,
        id: uid,
        disabled: disabled,
        'aria-describedby': noticeId
    },
    hasFocus: focused,
    optgroup: options,
    value: value,
    optionsCaption: caption,
    optionsValue: 'value',
    optionsText: 'label'</value>
      <webElementGuid>1b25ada6-9769-4d8e-99ae-9585c8cc8755</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>status</value>
      <webElementGuid>a3d93a9c-f1d8-4412-917c-0033a3b53622</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>SBQMQAD</value>
      <webElementGuid>6f48d3e1-d2db-4bca-bd4c-3cfcab7765ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>notice-SBQMQAD</value>
      <webElementGuid>9e4cb430-2b97-4be1-a223-3c32d5879985</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>PendingInactiveActiveRejected</value>
      <webElementGuid>1963a334-8823-4511-a463-00ba49d91271</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;SBQMQAD&quot;)</value>
      <webElementGuid>68caa3dd-7474-4c6c-bce2-b26a1ae367d7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='SBQMQAD']</value>
      <webElementGuid>c7c1c6a4-3ea5-4f39-9e53-350851efd686</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div[2]/div/div[2]/fieldset/div[3]/div[2]/select</value>
      <webElementGuid>7810388f-7c92-48fc-8181-c9a52b4a46ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[1]/following::select[1]</value>
      <webElementGuid>8424a1fc-f53d-4523-96a8-04fb0f052307</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Company Name'])[1]/following::select[1]</value>
      <webElementGuid>cbeeaf36-e5c1-49f0-a5ab-0d1ac28eacaa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rejected On'])[1]/preceding::select[1]</value>
      <webElementGuid>0a151cfd-9e4a-476c-93ca-d385d2afd94c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select Date'])[1]/preceding::select[1]</value>
      <webElementGuid>293becc9-46f9-4eb9-9f16-4703c3ed0086</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>b8c4f9a7-2834-4b42-b8f5-6fcc155ffa09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'status' and @id = 'SBQMQAD' and (text() = 'PendingInactiveActiveRejected' or . = 'PendingInactiveActiveRejected')]</value>
      <webElementGuid>b5988d71-646f-410b-a79d-cba12a2cd90a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
