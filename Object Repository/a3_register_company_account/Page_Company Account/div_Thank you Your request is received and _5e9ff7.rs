<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Thank you Your request is received and _5e9ff7</name>
   <tag></tag>
   <elementGuidId>370a4595-1cdb-41d8-b395-635aa3ce88fc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//main[@id='maincontent']/div/div[2]/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.message-success.success.message > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>38876154-a54f-41da-83bb-f92acd39d0a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>html: $parent.prepareMessageForHtml(message.text)</value>
      <webElementGuid>1dccd7f9-939b-48d5-b63d-d6632e8944d7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Thank you! Your request is received and will be reviewed as soon as possible max 1X24 hours. </value>
      <webElementGuid>535e562b-180e-4d88-a1e4-9556aee461fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;page messages&quot;]/div[2]/div[@class=&quot;messages&quot;]/div[@class=&quot;message-success success message&quot;]/div[1]</value>
      <webElementGuid>f94b0285-90d9-41dc-b27d-f0e9aeb7d219</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//main[@id='maincontent']/div/div[2]/div/div/div</value>
      <webElementGuid>61410cab-bd9b-46c4-b4bd-96f6c5b3882a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Compare'])[2]/following::div[6]</value>
      <webElementGuid>dd7149f1-c5df-42a2-848c-2b43f84f4cbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[4]/following::div[8]</value>
      <webElementGuid>a4009f73-bbfe-490d-8a7c-a2786c0526f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Company Account'])[4]/preceding::div[3]</value>
      <webElementGuid>53764c49-40c6-4faa-a9e2-4ff003a2c148</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Company Account'])[5]/preceding::div[5]</value>
      <webElementGuid>fedf2e68-8041-41eb-9d11-be8a6b4ae36a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Thank you! Your request is received and will be reviewed as soon as possible max 1X24 hours.']/parent::*</value>
      <webElementGuid>6b918ebb-e6c3-4a36-9491-ea4c38047248</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//main/div/div[2]/div/div/div</value>
      <webElementGuid>36f495aa-ca45-4dba-9c20-ac9d58b08fc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Thank you! Your request is received and will be reviewed as soon as possible max 1X24 hours. ' or . = 'Thank you! Your request is received and will be reviewed as soon as possible max 1X24 hours. ')]</value>
      <webElementGuid>966ed3bd-a743-4e2a-a5eb-7a436b3915f9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
