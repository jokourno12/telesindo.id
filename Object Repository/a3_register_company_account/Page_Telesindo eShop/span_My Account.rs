<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_My Account</name>
   <tag></tag>
   <elementGuidId>a1c83fa1-3d72-4400-a44e-5f2d407532b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.action.toggle.switcher-options</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(@class, 'action toggle switcher-options') and normalize-space()='My Account']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ea8273d6-21d9-4cf3-bb48-d7319151d53a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>action toggle switcher-options</value>
      <webElementGuid>4ffb75ae-6bec-4d91-b2e3-8221e6afe57f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>bf4db183-48aa-4fe9-a40a-138c4d7ef204</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>0484c0a6-415c-488a-ba28-caa0353c5a4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>12fb32dc-62fb-471a-9df9-f8b3dfd44ead</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>f4ad19bd-26ad-4e00-a644-2ab70c24ddeb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>4245891d-384b-4d55-a1b7-6c9bd54067b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                
            
            My Account        </value>
      <webElementGuid>29480b6a-a007-4273-9bca-298f2a65f413</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;html-body&quot;)/div[@class=&quot;page-wrapper&quot;]/header[@class=&quot;page-header&quot;]/div[@class=&quot;panel wrapper&quot;]/div[@class=&quot;panel header&quot;]/ul[@class=&quot;header links&quot;]/li[@class=&quot;amtheme-myaccount-link switcher&quot;]/span[@class=&quot;action toggle switcher-options&quot;]</value>
      <webElementGuid>642dd3a2-9bf0-47d4-8a8d-77edb06fa0db</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='html-body']/div[3]/header/div/div/ul/li[4]/span</value>
      <webElementGuid>a77b2b74-cf09-4699-a2ed-69befb50cab9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create a Company Account'])[1]/following::span[1]</value>
      <webElementGuid>f92e75b9-4ea7-4d03-8e12-977dce4c015a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quick Order'])[1]/following::span[1]</value>
      <webElementGuid>250f3e92-4fb2-4b72-8240-1ecfa187b23e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Account'])[1]/preceding::span[1]</value>
      <webElementGuid>3b654c85-9d1d-43c3-ab52-09d147e3beb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Company Account'])[1]/preceding::span[1]</value>
      <webElementGuid>b2ebc0c1-4f3e-4c83-ada4-f2d82acd6126</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='My Account']/parent::*</value>
      <webElementGuid>64bd5300-2cd8-45ef-ada8-f4f6cad55a2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/span</value>
      <webElementGuid>e342c7e5-5260-4408-966a-322edce247b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
            
                
            
            My Account        ' or . = '
            
                
            
            My Account        ')]</value>
      <webElementGuid>8896591f-a174-4538-9dda-cd7b2d7df655</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
