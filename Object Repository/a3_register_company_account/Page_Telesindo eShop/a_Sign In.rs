<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign In</name>
   <tag></tag>
   <elementGuidId>9db67623-c86c-40c1-9c00-8e315073059a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='html-body']/div[3]/header/div/div/ul/li/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Sign In&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>e110fcc4-d0cb-417c-91ed-0afa7d4cdf29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://telesindo-staging.mediant.id/customer/account/login/referer/aHR0cHM6Ly90ZWxlc2luZG8tc3RhZ2luZy5tZWRpYW50LmlkLw%2C%2C/</value>
      <webElementGuid>8f614912-0fb8-4bae-a188-0c0194e42b41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Sign In</value>
      <webElementGuid>a8cd3857-f896-4eb7-882f-ba5e1b78db64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Sign In</value>
      <webElementGuid>950592cd-4dc0-4cf5-9b34-5885046a6995</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Sign In    </value>
      <webElementGuid>cf476c0e-64c9-45e5-89b5-6e9764361785</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;html-body&quot;)/div[@class=&quot;page-wrapper&quot;]/header[@class=&quot;page-header&quot;]/div[@class=&quot;panel wrapper&quot;]/div[@class=&quot;panel header&quot;]/ul[@class=&quot;header links&quot;]/li[@class=&quot;authorization-link&quot;]/a[1]</value>
      <webElementGuid>4bbdb0be-91b2-4b2c-a93e-8066a077f868</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='html-body']/div[3]/header/div/div/ul/li/a</value>
      <webElementGuid>70ade6df-9b0a-414b-ba70-daa1e839c8d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Sign In')]</value>
      <webElementGuid>95888c2f-1bc2-4e16-919a-c305700d29ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Compare'])[1]/following::a[1]</value>
      <webElementGuid>f8fed4c5-bac9-4255-90d4-572f29b792f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to Content'])[1]/following::a[2]</value>
      <webElementGuid>372f582b-3e2b-4412-a222-2d5af1da8281</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create an Account'])[1]/preceding::a[1]</value>
      <webElementGuid>a65274ed-8541-4d61-b93e-5460416e8cd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[1]/preceding::a[5]</value>
      <webElementGuid>74efd02b-11af-4b8c-9ac5-3d437bb9efbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign In']/parent::*</value>
      <webElementGuid>6b417574-378b-4d78-b9fa-c0785ed1f9a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://telesindo-staging.mediant.id/customer/account/login/referer/aHR0cHM6Ly90ZWxlc2luZG8tc3RhZ2luZy5tZWRpYW50LmlkLw%2C%2C/')]</value>
      <webElementGuid>6d4b33b3-eb22-4ec3-aaf1-369d3b91dac8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a</value>
      <webElementGuid>10b8b9bd-7ec0-467d-bf83-d6b481daf053</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://telesindo-staging.mediant.id/customer/account/login/referer/aHR0cHM6Ly90ZWxlc2luZG8tc3RhZ2luZy5tZWRpYW50LmlkLw%2C%2C/' and @title = 'Sign In' and (text() = '
        Sign In    ' or . = '
        Sign In    ')]</value>
      <webElementGuid>b026906a-dd7b-4670-91ad-aa3f09a5bbc9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
