<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Street Address_companystreet0</name>
   <tag></tag>
   <elementGuidId>1d4a4d66-935e-4e38-8a22-d3c10883c582</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='street0']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#street0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>47d2f286-c16b-43ef-9aff-7dd9e18c0af5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>1a62ef6d-2ec3-406d-8647-231be857db29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>company[street][0]</value>
      <webElementGuid>3f170703-1e4c-47d5-8cfe-fdb4f47fbbe3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>street0</value>
      <webElementGuid>4402975d-72df-4052-8066-c9006ac88e6c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Street Address</value>
      <webElementGuid>a67c3b27-b9d4-4026-91d2-f7eff66aaf91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-text</value>
      <webElementGuid>bfa29b92-1417-4e82-aa5b-90948b929deb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-validate</name>
      <type>Main</type>
      <value>{required:true}</value>
      <webElementGuid>33d17a8b-8d7c-4bb5-b837-854cf2d712b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>68afa809-6140-4d70-90d1-6326f5d82a6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;street0&quot;)</value>
      <webElementGuid>86cb6f49-3d60-4d69-aacb-60224718c76f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='street0']</value>
      <webElementGuid>1a76fce9-bc96-46cc-9058-69e6025b9ad2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-validate']/fieldset[2]/div/div/input</value>
      <webElementGuid>90aaa447-276a-42a9-9e7b-3b52720cc978</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[2]/div/div/input</value>
      <webElementGuid>62e4d11c-f2d1-427c-b5e8-81e405ccc2b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'company[street][0]' and @id = 'street0' and @title = 'Street Address']</value>
      <webElementGuid>672cbdd0-16cd-4cc4-927e-16dc4789273e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
