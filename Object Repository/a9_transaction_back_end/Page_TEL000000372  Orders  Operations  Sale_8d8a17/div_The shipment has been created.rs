<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_The shipment has been created</name>
   <tag></tag>
   <elementGuidId>00b3b60e-f48a-4600-94a3-a905e2c5b306</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='messages']/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.message.message-success.success > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>236b26bd-bbb0-4b97-929e-6fed9679bc45</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ui-id</name>
      <type>Main</type>
      <value>messages-message-success</value>
      <webElementGuid>212585e4-d2ff-4245-8710-54db4736e578</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>The shipment has been created.</value>
      <webElementGuid>2e4a16c6-b2a7-41e8-a24e-f7bd868b2738</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;messages&quot;)/div[@class=&quot;messages&quot;]/div[@class=&quot;message message-success success&quot;]/div[1]</value>
      <webElementGuid>2b0743ac-4a99-463d-9558-1da14d6e0a21</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='messages']/div/div/div</value>
      <webElementGuid>04f85597-649a-4991-ab85-aaaabf63a871</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Clone as Quote'])[1]/following::div[4]</value>
      <webElementGuid>19747e29-847e-4f60-84af-231ecd968d6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reorder'])[1]/following::div[4]</value>
      <webElementGuid>1a3d09f8-6bae-4929-b9c4-70f047c45472</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order &amp; Account Information'])[1]/preceding::div[2]</value>
      <webElementGuid>5a810500-03e1-4357-ade6-dfbcb5e7f9cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='The shipment has been created.']/parent::*</value>
      <webElementGuid>04d68a14-b857-4943-9e2d-4d9c55ae4789</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//main/div[2]/div/div/div</value>
      <webElementGuid>4287071e-f3bd-4171-a5fd-7132347d25f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'The shipment has been created.' or . = 'The shipment has been created.')]</value>
      <webElementGuid>0f276d4f-d262-4487-b3b2-45e40acedef0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
