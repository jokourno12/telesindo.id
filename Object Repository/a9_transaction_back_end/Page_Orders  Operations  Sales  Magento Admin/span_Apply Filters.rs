<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Apply Filters</name>
   <tag></tag>
   <elementGuidId>dd8464fb-0db7-4cdf-a9a1-d082597d1708</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div/div[2]/div/div[5]/div/div/button[2]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.action-secondary > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>0ce88a26-bc46-4766-83e8-3e946d8e78ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>i18n: 'Apply Filters'</value>
      <webElementGuid>d7c60ba5-cfaa-45b6-b81b-3f612d617770</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Apply Filters</value>
      <webElementGuid>d09faf8c-4325-4dd3-a0a9-d3d542009ed4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[@class=&quot;admin__data-grid-outer-wrap&quot;]/div[@class=&quot;admin__data-grid-header&quot;]/div[@class=&quot;admin__data-grid-header-row&quot;]/div[@class=&quot;admin__data-grid-filters-wrap _show&quot;]/div[@class=&quot;admin__data-grid-filters-footer&quot;]/div[@class=&quot;admin__footer-main-actions&quot;]/button[@class=&quot;action-secondary&quot;]/span[1]</value>
      <webElementGuid>00199e9c-76bf-43fa-bc5f-56fe0b7a8fc6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div/div[2]/div/div[5]/div/div/button[2]/span</value>
      <webElementGuid>06bed442-8eaa-4257-ae4b-ac3c575cb47b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[3]/following::span[1]</value>
      <webElementGuid>b8d087b4-c192-4fab-9c89-4db105055b50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Company Name'])[2]/following::span[2]</value>
      <webElementGuid>40e490c5-6105-4ef2-a8e4-49935da8d06e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[1]/preceding::span[1]</value>
      <webElementGuid>64350cc8-b488-4198-a25d-eacd6662438c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[4]/preceding::span[2]</value>
      <webElementGuid>cce8427a-a746-4ec1-be5a-38ffc6ea20d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Apply Filters']/parent::*</value>
      <webElementGuid>d016ac67-d76c-4e94-b10e-f828d90e591e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]/span</value>
      <webElementGuid>6f827b8c-74ed-459a-85d2-409115d904de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Apply Filters' or . = 'Apply Filters')]</value>
      <webElementGuid>dd11133e-a652-4e25-b0f3-2b3019180dd8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
