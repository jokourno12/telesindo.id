<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Close</name>
   <tag></tag>
   <elementGuidId>068efc30-ab45-42ca-be84-36a1057264cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.ui-datepicker-close.ui-state-default.ui-priority-primary.ui-corner-all.ui-state-hover</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[text()='Close']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a70d5494-746c-4b86-9ddf-a5e6de845f2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5f438a82-8549-41ce-9acf-2bbc681a06f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover</value>
      <webElementGuid>b0b5267e-baad-4041-85b9-3db4853c4300</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-handler</name>
      <type>Main</type>
      <value>hide</value>
      <webElementGuid>a95c7fc6-b4cb-4169-9877-fb2c9ce2a623</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-event</name>
      <type>Main</type>
      <value>click</value>
      <webElementGuid>a436111a-3157-4356-9f82-035a700defcf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Close</value>
      <webElementGuid>5895b00b-4024-4d59-bb36-760a8410bc9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ui-datepicker-div&quot;)/div[@class=&quot;ui-datepicker-buttonpane ui-widget-content&quot;]/button[@class=&quot;ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover&quot;]</value>
      <webElementGuid>34c5bb03-14ae-4735-8f2a-b9cd3d96643d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[59]</value>
      <webElementGuid>1213ef2e-ada7-4d37-9c7f-91d1574cfea0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ui-datepicker-div']/div[2]/button[2]</value>
      <webElementGuid>4be67253-a3fc-44e6-b652-a07efe089145</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Go Today'])[1]/following::button[1]</value>
      <webElementGuid>8f2db9a6-201e-4b5f-9848-c13c7f50ff01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sun'])[1]/following::button[2]</value>
      <webElementGuid>5592943c-5d65-4415-9f82-0b231b05aa7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div[2]/button[2]</value>
      <webElementGuid>c639c64b-03de-4f91-a0ff-eef6a39ff9bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Close' or . = 'Close')]</value>
      <webElementGuid>28c5e8d4-a7b6-4acb-9687-aa909545a7ed</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
