<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Submit Shipment</name>
   <tag></tag>
   <elementGuidId>18220e07-bd6a-411d-a8ae-b8e9613c64d8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#id_pmp8AGTwMhC3dQBAa6xJCyJhI4cayvs0 > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[span[text()='Submit Shipment']]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>41d9cf1d-b1de-4920-af68-31383d795d4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit Shipment</value>
      <webElementGuid>cbf1ebed-4110-4315-a3ad-2820ad86677e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;id_pmp8AGTwMhC3dQBAa6xJCyJhI4cayvs0&quot;)/span[1]</value>
      <webElementGuid>2e76034d-ad77-475a-b3ac-a35997bc1f03</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='id_pmp8AGTwMhC3dQBAa6xJCyJhI4cayvs0']/span</value>
      <webElementGuid>50f2bf74-c825-4228-b507-b3a9bc0d521c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email Copy of Shipment'])[1]/following::span[1]</value>
      <webElementGuid>d33e4645-ebd6-4297-ae1b-4f9d620fa16a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Append Comments'])[1]/following::span[2]</value>
      <webElementGuid>de645439-ecf4-4408-959a-738f02c457d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Magento'])[1]/preceding::span[1]</value>
      <webElementGuid>f04403d5-5733-4962-a512-329c5744c624</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Submit Shipment']/parent::*</value>
      <webElementGuid>90b72019-a040-44fe-97c2-82b121812c2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button/span</value>
      <webElementGuid>9254831c-ba6c-4a9c-bc4f-b3b06673a366</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Submit Shipment' or . = 'Submit Shipment')]</value>
      <webElementGuid>baa1f20e-b3e5-48de-9300-8789d3bd28d2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
