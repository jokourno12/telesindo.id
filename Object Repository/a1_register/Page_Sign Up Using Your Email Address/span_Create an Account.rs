<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Create an Account</name>
   <tag></tag>
   <elementGuidId>6761b90f-03aa-4293-a745-9f2ee6dfe51a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form-validate']/div/div/button/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.action.submit.primary > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>4fe13cdb-d537-4fa7-b9d4-93d86f1c5a73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create an Account</value>
      <webElementGuid>190c0cf2-3869-45c6-b724-9341e2e67e44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-validate&quot;)/div[@class=&quot;actions-toolbar&quot;]/div[@class=&quot;primary&quot;]/button[@class=&quot;action submit primary&quot;]/span[1]</value>
      <webElementGuid>963fe6df-8dff-4ece-94a6-7efc6ce3e29d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-validate']/div/div/button/span</value>
      <webElementGuid>ef4c8f22-2350-4c6e-82fa-b9924dbf9511</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Show Password'])[1]/following::span[1]</value>
      <webElementGuid>fd779f8b-7ee7-41c6-8612-99f30a1b85ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm Password'])[1]/following::span[2]</value>
      <webElementGuid>1e8a21db-a41b-4583-ac2b-870bd3fe52f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/preceding::span[1]</value>
      <webElementGuid>1625b4b4-28d4-405e-8e03-18ede45fb672</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Subscribe to our newsletter'])[1]/preceding::span[2]</value>
      <webElementGuid>fb80803b-2114-4f70-a556-6d00a97d5997</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button/span</value>
      <webElementGuid>8720b114-de6b-4bf7-a5b4-87c6eb7c62e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Create an Account' or . = 'Create an Account')]</value>
      <webElementGuid>9b86afd1-863e-4acc-833c-cf2e5cb6960b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
