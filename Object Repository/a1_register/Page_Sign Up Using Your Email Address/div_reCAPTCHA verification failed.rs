<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_reCAPTCHA verification failed</name>
   <tag></tag>
   <elementGuidId>78056365-ea71-4436-bf4b-94f919d431da</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//main[@id='maincontent']/div[2]/div[2]/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.message-error.error.message > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4e06cebe-ac9c-4b77-beff-cf3bb246f7c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>html: $parent.prepareMessageForHtml(message.text)</value>
      <webElementGuid>0aeeabee-32be-4330-9928-fe077d86f822</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>reCAPTCHA verification failed.</value>
      <webElementGuid>8f387457-d0b5-4656-9ddc-852d4a250a34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;page messages&quot;]/div[2]/div[@class=&quot;messages&quot;]/div[@class=&quot;message-error error message&quot;]/div[1]</value>
      <webElementGuid>6a228e68-ee13-4588-8044-3740aaeae172</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//main[@id='maincontent']/div[2]/div[2]/div/div/div</value>
      <webElementGuid>f584b7a3-934c-4b78-9e50-fad1dcd1141a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Up Using Your Email Address'])[2]/following::div[6]</value>
      <webElementGuid>f62d2829-f446-4652-ade8-e54bb407787e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Compare'])[2]/following::div[7]</value>
      <webElementGuid>8069c2f8-3bca-4f97-a1b0-780a55b9a695</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Personal Information'])[1]/preceding::div[4]</value>
      <webElementGuid>3ffe961a-7191-4ca9-8a29-f0f1fc6f66ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First Name'])[1]/preceding::div[4]</value>
      <webElementGuid>28362520-d4a2-4cdd-9445-d4c1dee31dc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='reCAPTCHA verification failed.']/parent::*</value>
      <webElementGuid>3187e8ab-c7f0-4955-aaea-2174f974eb25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div</value>
      <webElementGuid>6bce15d8-680e-4c46-b1ca-365c67e2215b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'reCAPTCHA verification failed.' or . = 'reCAPTCHA verification failed.')]</value>
      <webElementGuid>6395f690-d9f9-42fb-9ef4-f3a132f033fe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
