<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Email_email</name>
   <tag></tag>
   <elementGuidId>b036233f-de95-49c2-81aa-2bc51bf08b66</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='email_address']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#email_address</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>771b001d-8e8c-4d18-899d-18ef88d12209</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>8577776d-ffe4-44f8-8c92-716f84ba4cf0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>04592f95-ce68-4d5c-81c1-7bb506cc780a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>1bc411ac-c47f-4490-89df-34d1fc1cb8d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>email_address</value>
      <webElementGuid>24ee5c4c-79f0-49cb-8873-7f8a0933f6c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Email</value>
      <webElementGuid>5bb02782-1441-4bcd-9921-0766585487a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-text</value>
      <webElementGuid>d4ad28cd-11d7-4e68-9fd7-2634270a8aaa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-validate</name>
      <type>Main</type>
      <value>{required:true, 'validate-email':true}</value>
      <webElementGuid>06c3ee96-1df5-4e7a-ba19-d14f1c3722f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>42bc983a-9e19-4a3e-b5d1-1c2ea1b058be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;email_address&quot;)</value>
      <webElementGuid>74b98f83-7fdb-41fa-a292-a3d0b8f76ee4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='email_address']</value>
      <webElementGuid>0e726329-d8a4-479c-9391-0e12dfa06b4e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-validate']/fieldset[2]/div/div/input</value>
      <webElementGuid>6b8fdc95-334f-4dd3-9953-f56b7d08b8cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//fieldset[2]/div/div/input</value>
      <webElementGuid>e87da0eb-0a55-4466-967d-f8c325206132</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'email' and @name = 'email' and @id = 'email_address' and @title = 'Email']</value>
      <webElementGuid>ca1d2226-e8f3-4bb6-8104-90af5c975688</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
