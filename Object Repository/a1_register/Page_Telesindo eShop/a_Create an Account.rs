<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Create an Account</name>
   <tag></tag>
   <elementGuidId>2ae0748f-db67-418f-953c-e7e5eba6297e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#idwZa2clTV</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[text()='Create an Account']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>47ed90ae-b181-4263-a4e6-939238c1cbc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://www.telesindo.id/customer/account/create/</value>
      <webElementGuid>6f506d66-aab8-4973-b427-aa5f1c429a04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>idwZa2clTV</value>
      <webElementGuid>c66d7a70-f9c8-4a46-be0f-ed4790d428c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create an Account</value>
      <webElementGuid>b69d9486-f9f0-4de0-bab6-f8088ef88697</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;idwZa2clTV&quot;)</value>
      <webElementGuid>b14b352a-66f5-4597-bbe4-f0f5713044d2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='idwZa2clTV']</value>
      <webElementGuid>7c0e3a74-7e03-426e-ad16-692df72b4059</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='html-body']/div[3]/header/div/div/ul/li[2]/a</value>
      <webElementGuid>13619fd9-caab-42da-a090-ae30fdba14bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Create an Account')]</value>
      <webElementGuid>efc6c1ad-fe93-4b1b-bbab-9b4517712704</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign In'])[1]/following::a[1]</value>
      <webElementGuid>8aa9b194-420b-4ac9-9a5c-e16f411cd2fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Compare'])[1]/following::a[2]</value>
      <webElementGuid>e02c43ea-d1bc-4bab-ae72-83255fb076d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[1]/preceding::a[4]</value>
      <webElementGuid>34554eb2-0b63-407c-a3bd-c7810a4b3504</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[2]/preceding::a[4]</value>
      <webElementGuid>c1e1a643-dd24-4ee8-9053-442b10dd5fb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Create an Account']/parent::*</value>
      <webElementGuid>d5e13990-247f-46a4-a960-ed8b82b7460f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://www.telesindo.id/customer/account/create/')]</value>
      <webElementGuid>2633dad6-38d1-412b-a2c0-b2df4674f878</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a</value>
      <webElementGuid>3fb1b1f5-b78c-421f-9759-b6eff3417521</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://www.telesindo.id/customer/account/create/' and @id = 'idwZa2clTV' and (text() = 'Create an Account' or . = 'Create an Account')]</value>
      <webElementGuid>db948075-e7c5-42da-94f1-c43e622cfbbb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
