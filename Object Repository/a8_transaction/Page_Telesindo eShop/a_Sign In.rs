<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign In</name>
   <tag></tag>
   <elementGuidId>38047a02-fc07-44fd-b183-93f04c70590d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='html-body']/div[3]/header/div/div/ul/li/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Sign In&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7e966a99-253e-4a13-805e-d877c5c3580d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://www.telesindo.id/customer/account/login/referer/aHR0cHM6Ly93d3cudGVsZXNpbmRvLmlkLw%2C%2C/</value>
      <webElementGuid>b7c1fcfe-c246-4857-b1da-340549dda481</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Sign In</value>
      <webElementGuid>99a29acd-a20f-44a7-bd0c-3033304c15a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Sign In</value>
      <webElementGuid>ead9b775-f460-43ce-a541-e8562f81d2cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Sign In    </value>
      <webElementGuid>2c694d23-9d75-437e-b9d5-2c7d19012971</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;html-body&quot;)/div[@class=&quot;page-wrapper&quot;]/header[@class=&quot;page-header&quot;]/div[@class=&quot;panel wrapper&quot;]/div[@class=&quot;panel header&quot;]/ul[@class=&quot;header links&quot;]/li[@class=&quot;authorization-link&quot;]/a[1]</value>
      <webElementGuid>50bf3aee-2b71-4772-92f2-dbf47130561f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='html-body']/div[3]/header/div/div/ul/li/a</value>
      <webElementGuid>9c49b2ec-53fb-4eea-ac73-b029ed53af0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Sign In')]</value>
      <webElementGuid>3529d041-3614-4013-9336-3ab948448d23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Compare'])[1]/following::a[1]</value>
      <webElementGuid>cc72a938-1a40-4a83-932c-084a301af372</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to Content'])[1]/following::a[2]</value>
      <webElementGuid>91064315-b514-4c52-b3dc-a63e88192e46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create an Account'])[1]/preceding::a[1]</value>
      <webElementGuid>95b0010e-8a87-476f-ad9c-991dcd76ea4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Cart,'])[1]/preceding::a[5]</value>
      <webElementGuid>e7464311-4b8d-4623-a142-0040b116a383</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign In']/parent::*</value>
      <webElementGuid>9d841bb2-f921-4ba2-abcc-d1d1a02e7ea8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://www.telesindo.id/customer/account/login/referer/aHR0cHM6Ly93d3cudGVsZXNpbmRvLmlkLw%2C%2C/')]</value>
      <webElementGuid>8e4259bb-468c-4fd4-9954-636cc6a37fc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a</value>
      <webElementGuid>6dd7c1f1-b647-4691-9345-b749e0d30903</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://www.telesindo.id/customer/account/login/referer/aHR0cHM6Ly93d3cudGVsZXNpbmRvLmlkLw%2C%2C/' and @title = 'Sign In' and (text() = '
        Sign In    ' or . = '
        Sign In    ')]</value>
      <webElementGuid>7b41ca2f-a5b8-456e-8332-15e9dcc8389e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
