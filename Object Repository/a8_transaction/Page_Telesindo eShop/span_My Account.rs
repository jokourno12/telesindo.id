<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_My Account</name>
   <tag></tag>
   <elementGuidId>e83a83b6-82e3-4f81-93f1-5fe99f96dfa5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.action.toggle.switcher-options</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(@class, 'action toggle switcher-options') and normalize-space()='My Account']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>6ca5c19e-bc0f-437e-94b8-e2626f265af1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>action toggle switcher-options</value>
      <webElementGuid>5caf7d26-7403-42ca-b299-8901829870d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>112b0303-e6f8-4bac-9cda-7faab657eb58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>12dd6520-742a-4a12-8e91-05f97baf349b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>4e00c912-7497-443f-b917-4c23f9a8f797</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>887ca033-c4fd-4187-aab8-fa0040cec441</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>d6145a3c-0a74-45dc-a0aa-498cc71286d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                
            
            My Account        </value>
      <webElementGuid>73b6fceb-aeb9-44f8-adc3-db8ced931e8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;html-body&quot;)/div[@class=&quot;page-wrapper&quot;]/header[@class=&quot;page-header&quot;]/div[@class=&quot;panel wrapper&quot;]/div[@class=&quot;panel header&quot;]/ul[@class=&quot;header links&quot;]/li[@class=&quot;amtheme-myaccount-link switcher&quot;]/span[@class=&quot;action toggle switcher-options&quot;]</value>
      <webElementGuid>f2b37610-e990-46cf-a732-b09c372cc24e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='html-body']/div[3]/header/div/div/ul/li[4]/span</value>
      <webElementGuid>aaab6125-2edb-43ce-92d7-e0b0f88d82d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create a Company Account'])[1]/following::span[1]</value>
      <webElementGuid>0ef5941a-1143-4173-8057-4ff683469051</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quick Order'])[1]/following::span[1]</value>
      <webElementGuid>54b69bec-503a-41aa-9164-64cc05ddb7c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Account'])[1]/preceding::span[1]</value>
      <webElementGuid>1e282176-3dca-47e5-9fe7-8219f1b9d7a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Company Account'])[1]/preceding::span[1]</value>
      <webElementGuid>dc1d0489-6712-4103-8c83-c04ff505582e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='My Account']/parent::*</value>
      <webElementGuid>a7b23721-4ffa-4047-87c3-bed6046a4cd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/span</value>
      <webElementGuid>46e08aaa-f379-4d00-9efa-185d2eca629f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
            
                
            
            My Account        ' or . = '
            
                
            
            My Account        ')]</value>
      <webElementGuid>232d373d-3c86-41dc-a322-e8fb9c1503b7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
