<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_My Quotes</name>
   <tag></tag>
   <elementGuidId>ce4f5263-b46d-4e79-a313-4c5e3e4b483b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@class='nav item']/a[text()='My Quotes']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ed5552fd-f0f6-4c8c-a71e-158b034f2fdb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://www.telesindo.id/request_quote/account/index/</value>
      <webElementGuid>a43f945c-3abd-4d69-8680-d4fc9b36c149</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>My Quotes</value>
      <webElementGuid>c9949c71-27b3-4dea-ba22-04e4df40ec5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;account-nav&quot;)/ul[@class=&quot;nav items&quot;]/li[@class=&quot;nav item&quot;]/a[1]</value>
      <webElementGuid>75dfccca-1859-456e-9533-8b36cd0d8aa4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='account-nav']/ul/li[3]/a</value>
      <webElementGuid>2c75671c-5d1f-4e96-bc77-346d4cb49cdd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'My Quotes')]</value>
      <webElementGuid>6bb324f1-aa61-489a-ba0c-215adf3aa3c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Orders'])[1]/following::a[1]</value>
      <webElementGuid>ec940e33-8090-4fbe-9d07-ed2273439392</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Account'])[3]/following::a[2]</value>
      <webElementGuid>b9c0dca9-9177-4133-882d-f81350a1b138</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Downloadable Products'])[1]/preceding::a[1]</value>
      <webElementGuid>64e0085a-6567-48ad-bcd7-f37fbeec792d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Wish List'])[1]/preceding::a[2]</value>
      <webElementGuid>aeac7059-3690-49b6-8364-619ae485142b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='My Quotes']/parent::*</value>
      <webElementGuid>14d2b15d-d11f-45e9-88a3-b7b83baf0505</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://www.telesindo.id/request_quote/account/index/')]</value>
      <webElementGuid>f65fe343-2d27-4986-8226-ab0589d57c75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[3]/a</value>
      <webElementGuid>6f9e7a11-2ca9-42ac-a20d-2bc8a42f2fa6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://www.telesindo.id/request_quote/account/index/' and (text() = 'My Quotes' or . = 'My Quotes')]</value>
      <webElementGuid>c6fea7ba-6d6c-4ed4-a91a-95ce802abec8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
