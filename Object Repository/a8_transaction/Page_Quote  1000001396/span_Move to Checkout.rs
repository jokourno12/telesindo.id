<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Move to Checkout</name>
   <tag></tag>
   <elementGuidId>746b9d9e-9e80-46e6-a081-e4e01e54b02d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='account-quote-buttons']//button[contains(span, 'Move to Checkout')]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7e873c94-e94a-48f3-90ff-49359b69cc7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Move to Checkout</value>
      <webElementGuid>2395dc41-e168-4c15-b11c-05f99523c163</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;columns&quot;]/div[@class=&quot;column main&quot;]/div[@class=&quot;account-quote-buttons&quot;]/button[@class=&quot;action tocart primary&quot;]/span[1]</value>
      <webElementGuid>fd6c1422-ce06-4683-9ea3-0ce6fbc4a879</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//main[@id='maincontent']/div[2]/div/div[5]/button[2]/span</value>
      <webElementGuid>910aaa95-2b5e-4db4-a733-88abb4f48c26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Move to Shopping Cart'])[1]/following::span[1]</value>
      <webElementGuid>af44b3a3-bba8-43ed-85b0-cc1bafdeb192</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Grand Total'])[1]/following::span[2]</value>
      <webElementGuid>48eb7b48-4a09-484f-a3cc-debe223cd656</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks'])[1]/preceding::span[1]</value>
      <webElementGuid>746f9b2a-16c0-47d5-9fe2-cf5c255c3856</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Move to Checkout']/parent::*</value>
      <webElementGuid>a406ce3b-8f64-4c31-a10e-0d3e97b36423</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]/span</value>
      <webElementGuid>5b1613ce-10e8-48d5-b6c6-147ff3934b71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Move to Checkout' or . = 'Move to Checkout')]</value>
      <webElementGuid>5a53c59d-7f91-4610-9936-8729596e1bc6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
