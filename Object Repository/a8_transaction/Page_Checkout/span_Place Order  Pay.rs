<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Place Order  Pay</name>
   <tag></tag>
   <elementGuidId>ffe9d4d7-b4c5-42c3-952c-665352c747a1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='place-order-trigger']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#place-order-trigger > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>81046743-60c8-47be-b964-bcac91baf9b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>text: 'Place Order &amp; Pay'</value>
      <webElementGuid>512d2285-abc0-4161-918f-073b2a382755</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Place Order &amp; Pay</value>
      <webElementGuid>ab8f0b02-57d9-4394-94b3-229c16c27407</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;place-order-trigger&quot;)/span[1]</value>
      <webElementGuid>b9998238-5317-485f-a363-b60379425521</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='place-order-trigger']/span</value>
      <webElementGuid>0fd9b1b4-2911-4c36-8534-30552e79a679</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[1]/following::span[1]</value>
      <webElementGuid>26934c02-949b-4667-93f1-f6b7c0bd268c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Do you have discount code?'])[1]/following::span[1]</value>
      <webElementGuid>0e81ec0f-4818-443e-a9ac-2a250a2f51a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order Summary'])[1]/preceding::span[1]</value>
      <webElementGuid>07882916-c528-484c-a92e-cb74a25a965f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Item in Cart'])[1]/preceding::span[4]</value>
      <webElementGuid>ba0a5b06-3b86-4281-810d-b281eda2c703</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/button/span</value>
      <webElementGuid>cb79789d-729d-4d27-b132-b6f21c3233d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Place Order &amp; Pay' or . = 'Place Order &amp; Pay')]</value>
      <webElementGuid>e55106c3-af85-41ee-86cb-cc5663f01376</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
