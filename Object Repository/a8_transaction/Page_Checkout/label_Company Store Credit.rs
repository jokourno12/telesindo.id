<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Company Store Credit</name>
   <tag></tag>
   <elementGuidId>a7dc0a51-85fb-40d1-b325-ad7797c2bb80</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.available-credit > label.label</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(span, 'Company Store Credit')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>18ed9fef-bea2-4aa1-9704-2c3c6122a9e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>attr: {'for': getCode()}</value>
      <webElementGuid>eed4736b-4380-42f4-8b9a-4603104b850e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>45b2ea12-33fa-448a-9f60-26c6233934ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>amasty_company_credit</value>
      <webElementGuid>2b10ec7a-8f65-4aac-a2e1-cebe06cdc0bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Company Store Credit
            </value>
      <webElementGuid>acdc12d8-2737-46f6-b9ac-810f349ae86e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-method-load&quot;)/div[@class=&quot;items payment-methods&quot;]/div[@class=&quot;payment-group&quot;]/div[@class=&quot;payment-method-options&quot;]/div[@class=&quot;payment-method&quot;]/div[@class=&quot;payment-method-title field choice&quot;]/div[@class=&quot;available-credit&quot;]/label[@class=&quot;label&quot;]</value>
      <webElementGuid>06b879c7-21a3-4427-aff2-dac1df26b26e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout-payment-method-load']/div/div/div[2]/div/div/div/label</value>
      <webElementGuid>06ff5c53-f21d-42f6-8d14-ff5fb6856f8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment Method'])[1]/following::label[1]</value>
      <webElementGuid>c56d4c95-9c1c-4a06-a2fc-dd1e686da40b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment Information'])[1]/following::label[1]</value>
      <webElementGuid>76938a29-3c93-422d-8cd8-414a6839731f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Place Order'])[1]/preceding::label[1]</value>
      <webElementGuid>d7b60df6-b7d0-456c-9752-800cc7f86f2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/label</value>
      <webElementGuid>71742b52-6707-4f7a-9617-10d069f97e3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = '
                Company Store Credit
            ' or . = '
                Company Store Credit
            ')]</value>
      <webElementGuid>22ca6af4-a531-4e47-9147-8f6792da19dd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
