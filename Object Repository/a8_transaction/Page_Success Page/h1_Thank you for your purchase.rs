<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Thank you for your purchase</name>
   <tag></tag>
   <elementGuidId>d9c284fd-bd57-4d07-94b8-35acabc95c1d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//main[@id='maincontent']/div[2]/div/div[2]/h1</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h1.page-title</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>fad5fdb5-3fc1-47a9-a461-c6788809c9ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>page-title</value>
      <webElementGuid>915ca433-4ae8-4574-811b-3c606cdea108</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ui-id</name>
      <type>Main</type>
      <value>page-title-wrapper</value>
      <webElementGuid>581afa09-a110-4126-ad58-288a20d03be3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Thank you for your purchase!        </value>
      <webElementGuid>c5f60db5-89b7-44ac-8130-80190b4257f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;columns&quot;]/div[@class=&quot;column main&quot;]/div[@class=&quot;page-title-wrapper&quot;]/h1[@class=&quot;page-title&quot;]</value>
      <webElementGuid>f3a975e5-4cbc-4130-b9fe-27d8048f40c8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//main[@id='maincontent']/div[2]/div/div[2]/h1</value>
      <webElementGuid>6c09cbfa-2b3f-45ec-a46c-f600534b9d95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Overview'])[1]/following::h1[1]</value>
      <webElementGuid>81fb4ecf-0908-44af-b7a3-78f49014fb47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='...'])[1]/following::h1[1]</value>
      <webElementGuid>11174033-3208-4e7a-8873-72c45c9f26ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TEL000000364'])[1]/preceding::h1[1]</value>
      <webElementGuid>bab65060-5b77-465d-9e6d-f61a9c888e42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Thank you for your purchase!']/parent::*</value>
      <webElementGuid>a4afeb14-1383-4a4e-8788-24895180737d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>ecc82823-db30-4ea6-9854-42df42181709</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = '
            Thank you for your purchase!        ' or . = '
            Thank you for your purchase!        ')]</value>
      <webElementGuid>aa7cfaf1-f10b-45a3-8b19-178e4d4e3bb3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
