<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_My Quote                    1            _b14acd</name>
   <tag></tag>
   <elementGuidId>a77184aa-8388-4a2f-a06c-884d17cb156d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.amquote-showcart.action</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='html-body']/div[3]/header/div[2]/div/div[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>c0c8938e-ec26-4aec-880f-b53c3733d4a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>amquote-showcart action</value>
      <webElementGuid>9c907988-3136-4d10-909d-9b69ef42bdc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://www.telesindo.id/request_quote/cart/</value>
      <webElementGuid>4902868f-ebc7-4054-8ad9-611597b260af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>scope: 'quotecart_content'</value>
      <webElementGuid>f20b4476-59f2-434a-964c-1724646eb4ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>My Quote</value>
      <webElementGuid>1d4ca78d-7a86-4c94-bb96-e20b1a63c47f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        My Quote
        
            1
            
            
                1
                items
                
            
        
    </value>
      <webElementGuid>f575ce85-f052-4861-9a36-a6aa31b2a796</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;html-body&quot;)/div[@class=&quot;page-wrapper&quot;]/header[@class=&quot;page-header&quot;]/div[@class=&quot;header content&quot;]/div[@class=&quot;amtheme-icons-container&quot;]/div[@class=&quot;amquote-cart-wrapper minicart-wrapper&quot;]/a[@class=&quot;amquote-showcart action&quot;]</value>
      <webElementGuid>362ce1fb-8cc5-4cb6-b786-783e9fb690ab</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='html-body']/div[3]/header/div[2]/div/div[2]/a</value>
      <webElementGuid>700a9256-51eb-4cd8-853c-ef8ddc9c5d31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[2]/following::a[3]</value>
      <webElementGuid>79eada4b-cb3f-47a2-8969-e2e773613c2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Company Account'])[1]/following::a[4]</value>
      <webElementGuid>377f02b3-70c1-4cfe-aa65-0630e4d66a44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://www.telesindo.id/request_quote/cart/')]</value>
      <webElementGuid>2778c807-9a16-428f-bb64-f84b936c06cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/a</value>
      <webElementGuid>7a8e55ba-d256-4255-9142-8752b48108cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://www.telesindo.id/request_quote/cart/' and @title = 'My Quote' and (text() = '
        My Quote
        
            1
            
            
                1
                items
                
            
        
    ' or . = '
        My Quote
        
            1
            
            
                1
                items
                
            
        
    ')]</value>
      <webElementGuid>3c7741bf-e095-4a16-a9d7-6e7ec98e4605</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
