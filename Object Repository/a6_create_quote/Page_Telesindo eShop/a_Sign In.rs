<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign In</name>
   <tag></tag>
   <elementGuidId>afa8f6f2-8dc6-4603-98cb-25f4f5e48abc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Sign In&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='html-body']/div[3]/header/div/div/ul/li/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>3e70980f-34fb-47f5-ad60-418b37570170</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://www.telesindo.id/customer/account/login/referer/aHR0cHM6Ly93d3cudGVsZXNpbmRvLmlkLw%2C%2C/</value>
      <webElementGuid>f4a002e6-3545-462f-9b5a-aa9a04731f9e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Sign In</value>
      <webElementGuid>dba7a99e-a9c5-452a-ae1d-4861799c54fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Sign In</value>
      <webElementGuid>59af7f60-6976-433b-bb0c-9963e69d81bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Sign In    </value>
      <webElementGuid>47a23bc1-fd5a-404f-80eb-c90f4817a9d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;html-body&quot;)/div[@class=&quot;page-wrapper&quot;]/header[@class=&quot;page-header&quot;]/div[@class=&quot;panel wrapper&quot;]/div[@class=&quot;panel header&quot;]/ul[@class=&quot;header links&quot;]/li[@class=&quot;authorization-link&quot;]/a[1]</value>
      <webElementGuid>ce4a769a-3650-4e0e-8b2f-1a67aa5c869d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='html-body']/div[3]/header/div/div/ul/li/a</value>
      <webElementGuid>46abf603-7ab0-4d2d-90c1-c2db8dc79481</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Sign In')]</value>
      <webElementGuid>e3fb9efd-638b-4a54-a01b-0be3ec9bcf6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Compare'])[1]/following::a[1]</value>
      <webElementGuid>5d13c87e-426f-44f5-b5d5-69282834bcad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to Content'])[1]/following::a[2]</value>
      <webElementGuid>142e9206-37fa-42ab-9e46-bc9e076adbc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create an Account'])[1]/preceding::a[1]</value>
      <webElementGuid>3554de1e-623a-49e8-a844-0bd923bc0005</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Cart,'])[1]/preceding::a[5]</value>
      <webElementGuid>47095871-f59b-41ea-9734-5f1c314767f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign In']/parent::*</value>
      <webElementGuid>0421a378-e9ca-4080-98fe-c67bfcafd7bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://www.telesindo.id/customer/account/login/referer/aHR0cHM6Ly93d3cudGVsZXNpbmRvLmlkLw%2C%2C/')]</value>
      <webElementGuid>efe09bef-811b-42c4-b922-7668945243fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a</value>
      <webElementGuid>ca133634-e502-4544-b948-62dcb4eee829</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://www.telesindo.id/customer/account/login/referer/aHR0cHM6Ly93d3cudGVsZXNpbmRvLmlkLw%2C%2C/' and @title = 'Sign In' and (text() = '
        Sign In    ' or . = '
        Sign In    ')]</value>
      <webElementGuid>54dc54fd-dfd2-458c-92d6-7e85e8362769</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
