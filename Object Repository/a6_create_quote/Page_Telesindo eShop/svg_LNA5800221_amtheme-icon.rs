<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_LNA5800221_amtheme-icon</name>
   <tag></tag>
   <elementGuidId>50d9003b-ce7e-4f24-9f82-f3b759eea7d8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.action.search > svg.amtheme-icon</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='LNA5800221'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>53711c73-ce2b-4604-be5b-7a85a5ce62a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>amtheme-icon</value>
      <webElementGuid>dcf05946-6bb4-4246-8a07-db5587437eed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;search_mini_form&quot;)/div[@class=&quot;actions&quot;]/button[@class=&quot;action search&quot;]/svg[@class=&quot;amtheme-icon&quot;]</value>
      <webElementGuid>10282251-7460-40dd-aa4a-991a4c85b513</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='LNA5800221'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>9c688d15-0ce2-4a2b-ab72-af21c6dbb968</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[2]/following::*[name()='svg'][1]</value>
      <webElementGuid>89326e01-17f4-41d6-bcb5-c853713c94e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Telesindo'])[1]/preceding::*[name()='svg'][3]</value>
      <webElementGuid>15e65369-9039-4f51-9201-e561cae4f576</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Perangkat Listrik Rumah'])[1]/preceding::*[name()='svg'][3]</value>
      <webElementGuid>efdc1d42-82c5-4ae6-88fa-84357a484b3a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
