<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Leona - horizontal 2-gang frame - white -_c0ea77</name>
   <tag></tag>
   <elementGuidId>2421cdaf-99d2-42f8-92a9-a1013d11133b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.amqorder-title</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='amasty-shopby-product-list']/div[2]/section/div/ul/li/div/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>0be44e6b-aa01-4a2c-802e-7e1226e8e75d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>amqorder-title</value>
      <webElementGuid>e69392bc-27be-4f1c-bfc2-4b59fb1f46ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>
        text: $parent.name,
        css: { '-disabled': !$parent.product_url },
        attr: { href: $parent.product_url }</value>
      <webElementGuid>6a6d3455-d4bc-44fb-b646-98fc9b17a963</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://www.telesindo.id/leona-horizontal-2-gang-frame-white-lna5800221/</value>
      <webElementGuid>3c69ff8b-7a7b-4e18-9d09-66c39a69ec60</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Leona - horizontal 2-gang frame - white - LNA5800221</value>
      <webElementGuid>848f6e7b-bca6-4dd6-8f3b-193d0a40f14b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;amasty-shopby-product-list&quot;)/div[@class=&quot;amqorder-products-wrapper products wrapper&quot;]/section[@class=&quot;amqorder-grid-block&quot;]/div[@class=&quot;amqorder-content&quot;]/ul[@class=&quot;amqorder-items-block&quot;]/li[@class=&quot;amqorder-item amqorder-item-info product-item-info -simple&quot;]/div[@class=&quot;amqorder-items-wrapper&quot;]/div[@class=&quot;amqorder-item -name&quot;]/a[@class=&quot;amqorder-title&quot;]</value>
      <webElementGuid>323715ce-e1ef-4329-bce3-421720745d1e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='amasty-shopby-product-list']/div[2]/section/div/ul/li/div/div/a</value>
      <webElementGuid>a973158d-efea-45d3-bcba-451c6f9c6baa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Leona - horizontal 2-gang frame - white - LNA5800221')]</value>
      <webElementGuid>896fdb1c-2eac-4bf7-91e1-a3f56219aa24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Schneider Electric'])[1]/following::a[1]</value>
      <webElementGuid>2023945f-a127-46d4-9094-5ee639cc290a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to Cart'])[1]/following::a[1]</value>
      <webElementGuid>7c20f219-d285-4d9e-8e04-fd277b9dfc64</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Option details'])[1]/preceding::a[1]</value>
      <webElementGuid>c4e3110e-e448-43f7-afc7-81e4a02885d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='LNA5800221'])[1]/preceding::a[1]</value>
      <webElementGuid>18d018e9-5a54-4000-bd9b-49e583fe14bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Leona - horizontal 2-gang frame - white - LNA5800221']/parent::*</value>
      <webElementGuid>8e6adc86-d63d-4466-86d6-02de969cfd23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://www.telesindo.id/leona-horizontal-2-gang-frame-white-lna5800221/')]</value>
      <webElementGuid>f160e6fa-abd2-4df2-8058-873b6985640e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div/div/a</value>
      <webElementGuid>8774ce90-7035-4402-bd20-bdf9890201df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://www.telesindo.id/leona-horizontal-2-gang-frame-white-lna5800221/' and (text() = 'Leona - horizontal 2-gang frame - white - LNA5800221' or . = 'Leona - horizontal 2-gang frame - white - LNA5800221')]</value>
      <webElementGuid>c44eee7e-a02a-4533-a90e-5ad55a256004</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
