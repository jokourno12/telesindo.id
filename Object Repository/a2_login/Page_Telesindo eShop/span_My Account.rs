<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_My Account</name>
   <tag></tag>
   <elementGuidId>ae6fb2f3-c37b-4c8c-860b-f393a8ce0def</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.action.toggle.switcher-options</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(@class, 'action toggle switcher-options') and normalize-space()='My Account']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>da853890-cf51-4e81-b2fb-65bf2e708a50</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>action toggle switcher-options</value>
      <webElementGuid>f43ab858-90b3-4dd0-8e0f-9524bca4cab5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>1ceac973-9bab-4d36-81ec-84de9d11f844</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>0e2a45d7-e3ae-4eae-9327-46a9c5235f5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>252336e8-0305-433e-adba-abb73563527c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>39e3153c-2c1d-4f46-8ac0-2201cda6604e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>c58ae0b1-febb-4441-9f93-4c188ae757ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                
            
            My Account        </value>
      <webElementGuid>d1fe33a9-cd9a-476f-b57a-4f7a7938daa7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;html-body&quot;)/div[@class=&quot;page-wrapper&quot;]/header[@class=&quot;page-header&quot;]/div[@class=&quot;panel wrapper&quot;]/div[@class=&quot;panel header&quot;]/ul[@class=&quot;header links&quot;]/li[@class=&quot;amtheme-myaccount-link switcher&quot;]/span[@class=&quot;action toggle switcher-options&quot;]</value>
      <webElementGuid>bde3e61a-e935-4277-b4f2-15fec4493c21</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='html-body']/div[3]/header/div/div/ul/li[3]/span</value>
      <webElementGuid>7fcec0eb-b9ba-4596-8e7a-f5e0cec1a142</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quick Order'])[1]/following::span[1]</value>
      <webElementGuid>6c24369a-6035-4496-8fab-14b82214e70c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[1]/following::span[1]</value>
      <webElementGuid>b1ad7789-ea7a-4031-bdde-98373c1ea7d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Account'])[1]/preceding::span[1]</value>
      <webElementGuid>eb2abcdc-4028-49ff-bf3b-d579f0f205e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Out'])[2]/preceding::span[1]</value>
      <webElementGuid>1fecb4dc-7937-4e49-a184-9caada94dc3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='My Account']/parent::*</value>
      <webElementGuid>bce7d732-d97e-47f7-84df-64b2b92823af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/span</value>
      <webElementGuid>050fa05a-9276-461a-95b8-6af8621f9d7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
            
                
            
            My Account        ' or . = '
            
                
            
            My Account        ')]</value>
      <webElementGuid>aa3c9066-c18d-4483-9ede-13cf08f31ea6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
