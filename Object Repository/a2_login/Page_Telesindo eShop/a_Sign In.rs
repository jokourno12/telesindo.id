<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign In</name>
   <tag></tag>
   <elementGuidId>6d715881-7db0-4f14-bde1-482c97761e3d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body[@id='html-body']/div[3]/header/div/div/ul/li/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Sign In&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>fc621d81-af69-4b7b-8f2d-d2b700dc0247</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://www.telesindo.id/customer/account/login/referer/aHR0cHM6Ly93d3cudGVsZXNpbmRvLmlkLw%2C%2C/</value>
      <webElementGuid>b4a5671c-4f89-4b8e-b1fa-6f439cbf70eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Sign In</value>
      <webElementGuid>3b24c448-3209-481d-aa7a-0a75381d8c4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Sign In</value>
      <webElementGuid>46aff9a6-7b32-4c2b-afa9-c78752b8f5de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Sign In    </value>
      <webElementGuid>638e40a1-ce29-4e0c-a3f1-4dd2e168452e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;html-body&quot;)/div[@class=&quot;page-wrapper&quot;]/header[@class=&quot;page-header&quot;]/div[@class=&quot;panel wrapper&quot;]/div[@class=&quot;panel header&quot;]/ul[@class=&quot;header links&quot;]/li[@class=&quot;authorization-link&quot;]/a[1]</value>
      <webElementGuid>d8e92d3f-749d-43e4-b07c-4585e1ddb877</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//body[@id='html-body']/div[3]/header/div/div/ul/li/a</value>
      <webElementGuid>12f332f6-7876-420e-a543-3415d23bb1d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Sign In')]</value>
      <webElementGuid>d2299628-9b57-4a94-927e-1b2ca25ea274</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Compare'])[1]/following::a[1]</value>
      <webElementGuid>b6106614-47bf-40b0-a1e9-df1e3c7295a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to Content'])[1]/following::a[2]</value>
      <webElementGuid>c70848d5-db9c-48b7-af4b-d30f7f18cee5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create an Account'])[1]/preceding::a[1]</value>
      <webElementGuid>7181917c-4886-4f2f-a895-4b08627c2183</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Cart,'])[1]/preceding::a[5]</value>
      <webElementGuid>87bc00e7-f8c7-48bb-a512-313f236de6d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign In']/parent::*</value>
      <webElementGuid>a0410a4d-dca2-4837-8f52-41ecea417cf7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://www.telesindo.id/customer/account/login/referer/aHR0cHM6Ly93d3cudGVsZXNpbmRvLmlkLw%2C%2C/')]</value>
      <webElementGuid>bd2f625f-19ff-465b-be7d-0792e2752691</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a</value>
      <webElementGuid>31d34b38-b3b9-47e5-a616-99f4bc66bc7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://www.telesindo.id/customer/account/login/referer/aHR0cHM6Ly93d3cudGVsZXNpbmRvLmlkLw%2C%2C/' and @title = 'Sign In' and (text() = '
        Sign In    ' or . = '
        Sign In    ')]</value>
      <webElementGuid>cf296aef-09df-43e9-9cb4-930041383dd1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
