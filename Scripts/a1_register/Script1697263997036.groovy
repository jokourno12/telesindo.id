import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.telesindo.id/')

WebUI.click(findTestObject('Object Repository/a1_register/Page_Telesindo eShop/a_Create an Account'))

WebUI.setText(findTestObject('Object Repository/a1_register/Page_Sign Up Using Your Email Address/input_First Name_firstname'), 
    'Joko')

WebUI.setText(findTestObject('Object Repository/a1_register/Page_Sign Up Using Your Email Address/input_Last Name_lastname'), 
    'Purnomo')

WebUI.setText(findTestObject('Object Repository/a1_register/Page_Sign Up Using Your Email Address/input_Email_email'), 'jokowirawangroup@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/a1_register/Page_Sign Up Using Your Email Address/input_Password_password'), 
    'TCdEHGtpZOAOScmyNwFgOw==')

WebUI.setEncryptedText(findTestObject('Object Repository/a1_register/Page_Sign Up Using Your Email Address/input_Confirm Password_password_confirmation'), 
    'TCdEHGtpZOAOScmyNwFgOw==')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/a1_register/Page_Sign Up Using Your Email Address/span_Create an Account'))

WebUI.verifyElementText(findTestObject('Object Repository/a1_register/Page_Sign Up Using Your Email Address/div_reCAPTCHA verification failed'), 
    'There is already an account with this email address. If you are sure that it is your email address, click here to get your password and access your account.')

WebUI.closeBrowser()

