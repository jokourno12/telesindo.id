import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.telesindo.id/')

WebUI.click(findTestObject('Object Repository/a6_create_quote/Page_Telesindo eShop/a_Sign In'))

WebUI.setText(findTestObject('Object Repository/a6_create_quote/Page_Customer Login/input_Email_loginusername'), 'jokowirawangroup@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/a6_create_quote/Page_Customer Login/input_Password_loginpassword'), 
    'TCdEHGtpZOAOScmyNwFgOw==')

WebUI.click(findTestObject('Object Repository/a6_create_quote/Page_Customer Login/button_Sign In'))

WebUI.setText(findTestObject('Object Repository/a6_create_quote/Page_Telesindo eShop/input_Search_q'), 'LNA5800221')

WebUI.click(findTestObject('Object Repository/a6_create_quote/Page_Telesindo eShop/svg_LNA5800221_amtheme-icon'))

WebUI.click(findTestObject('Object Repository/a6_create_quote/Page_Search results for LNA5800221/a_Leona - horizontal 2-gang frame - white -_c0ea77'))

WebUI.click(findTestObject('Object Repository/a6_create_quote/Page_Leona - horizontal 2-gang frame - whit_81498c/span_Add to Quote'))

WebUI.click(findTestObject('Object Repository/a6_create_quote/Page_Leona - horizontal 2-gang frame - whit_81498c/a_My Quote                    1            _b14acd'))

WebUI.click(findTestObject('Object Repository/a6_create_quote/Page_Leona - horizontal 2-gang frame - whit_81498c/button_My Quote Cart'))

WebUI.setText(findTestObject('Object Repository/a6_create_quote/Page_Quote Cart/textarea_Remarks_quote_remark'), 'Saya minta diskon 12%')

WebUI.click(findTestObject('Object Repository/a6_create_quote/Page_Quote Cart/button_Submit Quote'))

WebUI.verifyElementText(findTestObject('Object Repository/a6_create_quote/Page_Success Page/h1_Your quote request has been received'), 
    'Your quote request has been received!')

WebUI.closeBrowser()

