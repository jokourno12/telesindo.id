import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.telesindo.id/adminwufx59')

WebUI.setText(findTestObject('Object Repository/a4_activate_company_account_back_end/Page_Magento Admin/input_Username_loginusername'), 
    'jokourno12')

WebUI.setEncryptedText(findTestObject('Object Repository/a4_activate_company_account_back_end/Page_Magento Admin/input_Password_loginpassword'), 
    'TCdEHGtpZOAOScmyNwFgOw==')

WebUI.click(findTestObject('Object Repository/a4_activate_company_account_back_end/Page_Magento Admin/span_Sign in'))

WebUI.click(findTestObject('Object Repository/a4_activate_company_account_back_end/Page_Dashboard  Magento Admin/span_Customers'))

WebUI.click(findTestObject('Object Repository/a4_activate_company_account_back_end/Page_Dashboard  Magento Admin/span_Manage Companies'))

WebUI.click(findTestObject('Object Repository/a4_activate_company_account_back_end/Page_Company Accounts  Company Accounts  Cu_1b8efb/div_Aaaaaaa'))

WebUI.verifyElementText(findTestObject('Object Repository/a4_activate_company_account_back_end/Page_Edit Company Aaaaaaa  Company Accounts_665bd4/h1_Edit Company Aaaaaaa'), 
    'Edit Company "Aaaaaaa"')

WebUI.selectOptionByValue(findTestObject('Object Repository/a4_activate_company_account_back_end/Page_Edit Company Aaaaaaa  Company Accounts_665bd4/select_PendingInactiveActiveRejected'), 
    '1', true)

WebUI.click(findTestObject('Object Repository/a4_activate_company_account_back_end/Page_Edit Company Aaaaaaa  Company Accounts_665bd4/span_SaveClose'))

WebUI.verifyElementText(findTestObject('Object Repository/a4_activate_company_account_back_end/Page_Company Accounts  Company Accounts  Cu_1b8efb/div_You have saved the Company'), 
    'You have saved the Company.')

WebUI.closeBrowser()

