import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.telesindo.id/')

WebUI.click(findTestObject('Object Repository/a8_transaction/Page_Telesindo eShop/a_Sign In'))

WebUI.setText(findTestObject('Object Repository/a8_transaction/Page_Customer Login/input_Email_loginusername'), 'jokowirawangroup@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/a8_transaction/Page_Customer Login/input_Password_loginpassword'), 
    'TCdEHGtpZOAOScmyNwFgOw==')

WebUI.click(findTestObject('Object Repository/a8_transaction/Page_Customer Login/button_Sign In'))

WebUI.click(findTestObject('Object Repository/a8_transaction/Page_Telesindo eShop/span_My Account'))

WebUI.click(findTestObject('Object Repository/a8_transaction/Page_Telesindo eShop/a_Account'))

WebUI.click(findTestObject('Object Repository/a8_transaction/Page_My Account/a_My Quotes'))

WebUI.click(findTestObject('Object Repository/a8_transaction/Page_My Quotes/span_View'))

WebUI.click(findTestObject('Object Repository/a8_transaction/Page_Quote  1000001396/span_Move to Checkout'))

WebUI.click(findTestObject('Object Repository/a8_transaction/Page_Checkout/span_Next'))

WebUI.click(findTestObject('Object Repository/a8_transaction/Page_Checkout/label_Company Store Credit'))

WebUI.click(findTestObject('Object Repository/a8_transaction/Page_Checkout/span_Place Order  Pay'))

WebUI.verifyElementText(findTestObject('Object Repository/a8_transaction/Page_Success Page/h1_Thank you for your purchase'), 
    'Thank you for your purchase!')

WebUI.closeBrowser()

