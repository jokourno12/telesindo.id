import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.telesindo.id/adminwufx59')

WebUI.setText(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Magento Admin/input_Username_loginusername'), 
    'jokourno12')

WebUI.setEncryptedText(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Magento Admin/input_Password_loginpassword'), 
    'TCdEHGtpZOAOScmyNwFgOw==')

WebUI.click(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Magento Admin/span_Sign in'))

WebUI.click(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Dashboard  Magento Admin/span_Sales'))

WebUI.click(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Dashboard  Magento Admin/span_Quotes'))

WebUI.click(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Quotes  Amasty Request a Quote  Sales _23a443/button_Filters'))

WebUI.click(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Quotes  Amasty Request a Quote  Sales _23a443/button_Select Date'))

WebUI.click(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Quotes  Amasty Request a Quote  Sales _23a443/button_Go Today'))

WebUI.click(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Quotes  Amasty Request a Quote  Sales _23a443/button_Close'))

WebUI.click(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Quotes  Amasty Request a Quote  Sales _23a443/span_Apply Filters'))

WebUI.click(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Quotes  Amasty Request a Quote  Sales _23a443/a_View'))

WebUI.click(findTestObject('Object Repository/a7_accept_quote_back_end/Page_1000001419  Quotes  Amasty Request a Q_e18cf5/span_Edit'))

WebUI.setText(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Edit Quote  Quotes  Amasty Request a Q_d82626/input_Additional Discount ()_quotediscount'), 
    '12.00')

WebUI.click(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Edit Quote  Quotes  Amasty Request a Q_d82626/span_Update Items and Quantities'))

WebUI.click(findTestObject('Object Repository/a7_accept_quote_back_end/Page_Edit Quote  Quotes  Amasty Request a Q_d82626/span_Save Quote'))

WebUI.verifyElementText(findTestObject('Object Repository/a7_accept_quote_back_end/Page_1000001419  Quotes  Amasty Request a Q_e18cf5/div_You updated the quote'), 
    'You updated the quote.')

WebUI.click(findTestObject('Object Repository/a7_accept_quote_back_end/Page_1000001419  Quotes  Amasty Request a Q_e18cf5/span_Approve'))

WebUI.verifyElementText(findTestObject('Object Repository/a7_accept_quote_back_end/Page_1000001419  Quotes  Amasty Request a Q_e18cf5/div_You approved the quote'), 
    'You approved the quote.')

WebUI.closeBrowser()

