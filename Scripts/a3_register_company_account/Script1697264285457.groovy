import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://telesindo.id/')

WebUI.click(findTestObject('Object Repository/a3_register_company_account/Page_Telesindo eShop/a_Sign In'))

WebUI.setText(findTestObject('Object Repository/a3_register_company_account/Page_Customer Login/input_Email_loginusername'), 
    'jokowirawangroup@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/a3_register_company_account/Page_Customer Login/input_Password_loginpassword'), 
    'TCdEHGtpZOAOScmyNwFgOw==')

WebUI.click(findTestObject('Object Repository/a3_register_company_account/Page_Customer Login/span_Sign In'))

WebUI.click(findTestObject('Object Repository/a3_register_company_account/Page_Telesindo eShop/span_My Account'))

WebUI.click(findTestObject('Object Repository/a3_register_company_account/Page_Telesindo eShop/a_Company Account'))

WebUI.click(findTestObject('Object Repository/a3_register_company_account/Page_Company Account/a_Create'))

WebUI.setText(findTestObject('Object Repository/a3_register_company_account/Page_New Company Account/input_Company Name_companycompany_name'), 
    'Aaaaaaa')

WebUI.setText(findTestObject('Object Repository/a3_register_company_account/Page_New Company Account/input_Company Legal Name_companylegal_name'), 
    'Aaaaaaa')

WebUI.setText(findTestObject('Object Repository/a3_register_company_account/Page_New Company Account/input_Company Email_companycompany_email'), 
    'jokowirawangroup@gmail.com')

WebUI.setText(findTestObject('Object Repository/a3_register_company_account/Page_New Company Account/input_Street Address_companystreet0'), 
    'Jalan Tanah Seratus')

WebUI.setText(findTestObject('Object Repository/a3_register_company_account/Page_New Company Account/input_City_companycity'), 
    'Kota Tangerang')

WebUI.setText(findTestObject('Object Repository/a3_register_company_account/Page_New Company Account/input_StateProvince_companyregion'), 
    'Banten')

WebUI.setText(findTestObject('Object Repository/a3_register_company_account/Page_New Company Account/input_ZIPPostal Code_companypostcode'), 
    '15151')

WebUI.setText(findTestObject('Object Repository/a3_register_company_account/Page_New Company Account/input_Phone Number_companytelephone'), 
    '085888405053')

WebUI.click(findTestObject('Object Repository/a3_register_company_account/Page_New Company Account/span_Submit'))

WebUI.verifyElementText(findTestObject('Object Repository/a3_register_company_account/Page_Company Account/div_Thank you Your request is received and _5e9ff7'), 
    'Thank you! Your request is received and will be reviewed as soon as possible max 1X24 hours.')

WebUI.closeBrowser()

