import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.telesindo.id/adminwufx59')

WebUI.setText(findTestObject('Object Repository/a9_transaction_back_end/Page_Magento Admin/input_Username_loginusername'), 
    'jokourno12')

WebUI.setEncryptedText(findTestObject('Object Repository/a9_transaction_back_end/Page_Magento Admin/input_Password_loginpassword'), 
    'TCdEHGtpZOAOScmyNwFgOw==')

WebUI.click(findTestObject('Object Repository/a9_transaction_back_end/Page_Magento Admin/span_Sign in'))

WebUI.click(findTestObject('Object Repository/a9_transaction_back_end/Page_Dashboard  Magento Admin/span_Sales'))

WebUI.click(findTestObject('Object Repository/a9_transaction_back_end/Page_Dashboard  Magento Admin/span_Orders'))

WebUI.click(findTestObject('Object Repository/a9_transaction_back_end/Page_Orders  Operations  Sales  Magento Admin/button_Filters'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/a9_transaction_back_end/Page_Orders  Operations  Sales  Magento Admin/button_Select Date'))

WebUI.delay(3)

WebUI.click(findTestObject('a9_transaction_back_end/Page_Orders  Operations  Sales  Magento Admin/button_Go Today'))

WebUI.click(findTestObject('Object Repository/a9_transaction_back_end/Page_Orders  Operations  Sales  Magento Admin/button_Close'))

WebUI.click(findTestObject('Object Repository/a9_transaction_back_end/Page_Orders  Operations  Sales  Magento Admin/span_Apply Filters'))

WebUI.click(findTestObject('Object Repository/a9_transaction_back_end/Page_Orders  Operations  Sales  Magento Admin/button_Select'))

WebUI.click(findTestObject('Object Repository/a9_transaction_back_end/Page_Orders  Operations  Sales  Magento Admin/a_View'))

WebUI.click(findTestObject('Object Repository/a9_transaction_back_end/Page_TEL000000372  Orders  Operations  Sale_8d8a17/span_Ship'))

WebUI.click(findTestObject('Object Repository/a9_transaction_back_end/Page_New Shipment  Shipments  Operations  S_9cec8f/span_Submit Shipment'))

WebUI.verifyElementText(findTestObject('Object Repository/a9_transaction_back_end/Page_TEL000000372  Orders  Operations  Sale_8d8a17/div_The shipment has been created'), 
    'The shipment has been created.')

WebUI.closeBrowser()

